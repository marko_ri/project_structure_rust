fn main() {
    println!(
        "cargo run --example {}",
        std::path::Path::new(file!())
            .file_name()
            .and_then(|f| f.to_str())
            .unwrap()
    );
}
