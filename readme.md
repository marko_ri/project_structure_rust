
Experimenting with project structure in Rust. Taking inspiration from:
`Architecture Patterns with Python, by Harry Percival, Bob Gregory` and `Domain Modeling Made Functional, by Scott Wlaschin`

## Short Summary
- useful tools:
  - encapsulation, abstraction, dependency inversion
  - onion architecture (all dependencies must point inwards, shift IO to the edges)
  - make illegal states unrepresentable (capture business rules in type system)

- think about code in terms of behavior, rather than in terms of data or algorithms
  - i.e focus on events and processes rather than data
  - partition the problem domain into smaller sub-domains
- first focus on inputs and outputs of the workflow
  - the output of the workflow should always be the events that it generates, the things that trigger actions in other bounded contexts
- discuss the scale of the workflow

- entities are compared by id. They have a lifecycle, and are transformed from one state to another by various business processes.
- value objects are compared by their fields (they are immutable)
- aggregates enforce consistency and invariants
  - in single threaded application, it's relatively easy to maintain invariants. When concurrency is introduced,
    we usually solve this problem by applying locks to our database tables. But this does not scale
  - the aggregate will be the boundary where we make sure every operation ends in consistent state
    - use of optimistic locking (and version numbers) is one way: you can put version_number in the domain (on the aggregate root)
  - all changes to objects inside an aggregate must be applied via the top level, to the root
  - connect aggregates with references (i.e. aggregate ids)
  - aggregates are basic unit of persistence (database transaction should work with single aggregate), and data transfer
  - aggregates are the only entities accessible to the outside world !!
  - there isn't one correct aggregate (if it affects performance, choose another)
  - key consideration in choosing your aggregates is also choosing the bounded context that they will operate in
- bounded context could be a separate module with a well defined interface,
  or it could be deployed separately in its own container. Start with monolith
- bounded contexts communicate with events
  - you could place event on a queue (buffered asynchronous communication), or simply use function call
  - event should contain data (dto) that the downstream component needs to process the event
- anything inside the bounded context will be trusted and valid, while anything outside will be untrusted and might be invalid
  - input gate will always validate the input and provide anti corruption layer,
  - output gate will ensure that private data does not leak out
- keep consistency between different contexts
  - usually this is done asynchronously using messages (eventual consistency)
  - look out for times when things go wrong (e.g. detect that message was lost and resend it again, or if an order is canceled,
    find the products that were allocated to it and remove the allocations)

- represent the business workflow as pipeline - each step should be stateless and without side-effects
  (e.g. PlaceOrderWorkflow: unvalidatedOrder |> validateOrder |> priceOrder |> acknowledgeOrder |> createEvents)
  - input to the workflow should always be a domain object (deserialized from dto)
- model lifecycle of objects with state machines
- dealing with effects: make it clear in type signature that function is doing IO and that it might fail (async + result)
- are dependencies part of the design?
  - for functions exposed in a public API, hide dependency information from callers
  - for functions used internally, be explicit about their dependencies
- if external services need long time to execute save the state into db, wait for message that service has finished, reload state from db and continue with workflow

- events & commands
  - events can help with single responsibility principle.
  - one way to handle domain events: domain model raises events, service layer passes them to message bus
    - a message bus routes messages to handlers (you can think of it as a dictionary)

   |           | Events               |  Commands     |
   |-----------|----------------------|---------------|
   | named:    | past tense           | imperative    |
   | errors:   | fail independently   | fail noisily  |
   | sent_to:  | all listeners        | one recipient |

  - we do not require the event handlers to succeed in order for the command to be successful
  - events (that our workflow emits) and commands (we consume) form a contract that our bounded context must support -> change it carefully

- testing:
  - aim for one end-to-end test per feature (demonstrate that feature works with all the moving parts glued together)
    - error handling counts as feature
  - write the bulk of your tests against the service layer (use fakes for IO)
    - service layer: takes care of orchestrating workflows and defining use cases
  - maintain (a small core of) tests written against domain model (these are most brittle)

- errors:
  - if remote validation service is unavailable, how should the business process change? (what to show to customer?)
    - retry couple of times, keep some cached values, or just panic ?
  - model errors as enum

- serialization/deserialization
  - complex domain types are not well suited for serializer, so first convert domain objects to dtos (both ways: serialization and deserialization)
  - dtos consist of primitive types
  - deserialization to dto should always succeed (unless data is corrupted): any validation should be done in the dto -> domain type conversion process

- persistence
  - general guidelines: push persistence to the edges; separate commands (updates) from queries (reads); bounded contexts must own their own data store
  - in case all necessary data can not be loaded on the edge (e.g. you need to make a decision based on reading from db, in the middle of "pure" code)
    - keep the pure functions intact, but sandwich them between impure IO functions
    - if there is too much mixing of IO and logic, you might want to break the workflow into shorter mini workflows
  - Command-Query Separation:
    - functions that return data should not have side effects
    - functions that have side effects (updating state) should not return data
 (- it is not a good idea to reuse the same type for both reading and writing
    - e.g. when you load an order, you may also want to load customer data associated with that order (rather than making second trip to db)
    - when you are saving the order to db, you would use only the reference to the customer rather than the entire customer
 )
  - bounded context must own its own data storage and associated schemas. No other system can directly access data owned by the bounded context
    - Instead the client should use the public API of the bounded context, or use some kind of copy (by listening on certain events)
    - The implementation of isolation may vary: from physically different databases to only one database with some kind of namespace mechanism

   |               |   Read            |     Write                           |
   |---------------|-------------------|-------------------------------------|
   | behavior:     | simple read       |  complex business logic             |
   | cacheability: | highly cacheable  |  uncacheable                        |
   | consistency:  | can be stale      |  must be transactionally consistent |

  - domain model is not a data model - it captures the way business works, and it is not optimized for read operations
  - normalized relational tables are good way to insure that write operations do not cause data corruption. But retrieving data using lots of
    joins can be slow. It is common in such cases to add some denormalized views/build read replicas. This also helps with scaling (no need for
    a lock when reading from db), but keeping the read model up to date can be a challenge (triggers are a common solution, .. or extra event handlers).

- evolution
  - we should not model the output of a business rule in the domain (e.g. adding a "free shipping" flag to the order). Instead we should store
    the input to a business rule ("customer is a vip"), and then let the business rule work on that input. That way if business rule change
    we do not have to change our domain model
  - avoid modifying stable code, rather add another segment to the workflow pipeline
  - to avoid breaking the contract between different bounded contexts (events that get sent downstream from our bounded context), a good solution
    is to use "consumer driven" contracts: i.e. the downstream consumer decides what they need from the upstream producer, and producer must
    provide that and nothing more

- footguns
  - Reliable messaging is hard
  - good monitoring is needed to know when transactions fail (and good way to replay events)
  - handlers should be idempotent
  - document your events and changes of their schema