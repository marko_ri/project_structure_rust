-- PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS products
(
    id                TEXT PRIMARY KEY NOT NULL,
    version_number    INTEGER          NOT NULL
);

CREATE TABLE IF NOT EXISTS batches
(
    id                TEXT PRIMARY KEY NOT NULL,
    sku               TEXT             NOT NULL,
    purchased_qty     TEXT             NOT NULL,
    eta               INTEGER          NUll,
    FOREIGN KEY(sku) REFERENCES products(id)
       ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX IF NOT EXISTS batches_sku_index ON batches(sku);

CREATE TABLE IF NOT EXISTS order_lines
(
    id          INTEGER PRIMARY KEY NOT NULL,
    sku         TEXT                NOT NULL,
    qty         TEXT                NOT NULL,
    order_id    TEXT                NOT NUll,
    batch_id    TEXT                NOT NULL,
    FOREIGN KEY(batch_id) REFERENCES batches(id)
       ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX IF NOT EXISTS order_lines_batch_id_index ON order_lines(batch_id);
