#![allow(dead_code)]

pub mod bootstrap;
#[macro_use]
mod util;
pub mod adapter;
pub mod domain;
pub mod entrypoint;
pub(crate) mod service_layer;
