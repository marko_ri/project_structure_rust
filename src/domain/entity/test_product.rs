use crate::domain::{
    entity::{Batch, Product},
    error::{AllocationError, ValidationError},
    event::{AllocatedEvent, OutOfStockEvent},
    value_object::{DeliveryTime, Message, OrderLine},
};

#[test]
fn test_prefers_warehouse_batches_to_shipments() -> Result<(), AllocationError> {
    let now = DeliveryTime::now();
    let later = now.plus_secs(60);

    let in_stock_batch = Batch::try_new("in-warehouse-stock", "TABLE", "U10", None)?;
    let shipment_batch = Batch::try_new("shipment-batch", "TABLE", "U10", Some(later.to_secs()))?;
    let order_line = OrderLine::try_new("oref", "TABLE", "U2")?;

    let batches = vec![in_stock_batch, shipment_batch];
    let mut product = Product::try_new("TABLE", batches, 0)?;
    product.allocate(&order_line).unwrap();

    assert_eq!(
        8.0,
        product
            .batches()
            .iter()
            .find(|&b| b.id.value() == "in-warehouse-stock")
            .map(|b| b.available_quantity())
            .unwrap()
    );
    assert_eq!(
        10.0,
        product
            .batches()
            .iter()
            .find(|&b| b.id.value() == "shipment-batch")
            .map(|b| b.available_quantity())
            .unwrap()
    );

    Ok(())
}

#[test]
fn test_prefers_earlier_batches() -> Result<(), AllocationError> {
    let now = DeliveryTime::now();
    let later = now.plus_secs(60);

    let earliest = Batch::try_new("speedy-batch", "TABLE", "U10", Some(now.to_secs()))?;
    let latest = Batch::try_new("slow-batch", "TABLE", "U10", Some(later.to_secs()))?;
    let order_line = OrderLine::try_new("oref", "TABLE", "U2")?;

    let batches = vec![latest, earliest];
    let mut product = Product::try_new("TABLE", batches, 0)?;
    let br = product.allocate(&order_line).unwrap();

    assert_eq!("speedy-batch", br.value());

    Ok(())
}

#[test]
fn test_outputs_allocated_event() -> Result<(), AllocationError> {
    let sku = "RETRO-LAMPSHADE";
    let batch = Batch::try_new("batch01", sku, "U100", None)?;
    let order_line = OrderLine::try_new("oref", sku, "U10")?;
    let mut product = Product::try_new(sku, vec![batch], 0)?;
    product.allocate(&order_line).unwrap();
    let expected = Message::from(AllocatedEvent {
        order_id: "oref".to_string(),
        sku: sku.to_string(),
        qty: "U10".to_string(),
        batch_id: "batch01".to_string(),
    });
    assert_eq!(&expected, product.messages.last().unwrap());

    Ok(())
}

#[test]
fn test_records_out_of_stock_event_if_cannot_allocate() -> Result<(), ValidationError> {
    let sku = "TABLE";
    let batches = vec![Batch::try_new("batch1", sku, "U10", None)?];
    let mut product = Product::try_new(sku, batches, 0)?;

    let result = product.allocate(&OrderLine::try_new("oref1", sku, "U10")?);
    assert!(result.is_some());

    let result = product.allocate(&OrderLine::try_new("oref2", sku, "U1")?);
    assert!(result.is_none());
    assert_eq!(
        &Message::from(OutOfStockEvent {
            sku: sku.to_string()
        }),
        product.messages.last().unwrap()
    );

    Ok(())
}
