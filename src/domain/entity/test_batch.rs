use super::*;
use crate::domain::{error::ValidationError, value_object::DeliveryTime};

#[test]
fn entities_with_same_id_are_equal() -> Result<(), ValidationError> {
    let batch1 = Batch::try_new(
        "batch-001",
        "SMALL-TABLE",
        "U1",
        Some(DeliveryTime::now().to_secs()),
    )?;
    let batch2 = Batch::try_new("batch-001", "BIG-TABLE", "U2", None)?;
    assert_eq!(batch1, batch2);

    Ok(())
}

#[test]
fn entities_with_different_ids_are_not_equal() -> Result<(), ValidationError> {
    let batch1 = Batch::try_new(
        "batch-001",
        "SMALL-TABLE",
        "U1",
        Some(DeliveryTime::now().to_secs()),
    )?;
    let batch2 = Batch::try_new(
        "batch-002",
        "SMALL-TABLE",
        "U1",
        Some(DeliveryTime::now().to_secs()),
    )?;
    assert_ne!(batch1, batch2);

    Ok(())
}

#[test]
fn test_allocating_to_a_batch_reduces_the_available_quantity() -> Result<(), ValidationError> {
    let mut batch = Batch::try_new(
        "batch-001",
        "SMALL-TABLE",
        "U20",
        Some(DeliveryTime::now().to_secs()),
    )?;
    let order_line = OrderLine::try_new("oref", "SMALL-TABLE", "U2")?;

    batch.allocate(&order_line);

    assert_eq!(18.0, batch.available_quantity());

    Ok(())
}

#[test]
fn test_can_allocate_if_available_greater_than_required() -> Result<(), ValidationError> {
    let large_batch = Batch::try_new("batch-001", "ELEGANT-LAMP", "U20", None)?;
    let small_line = OrderLine::try_new("oref", "ELEGANT-LAMP", "U2")?;

    assert!(large_batch.can_allocate(&small_line));

    Ok(())
}

#[test]
fn test_cannot_allocate_if_available_smaller_than_required() -> Result<(), ValidationError> {
    let small_batch = Batch::try_new("batch-001", "ELEGANT-LAMP", "U2", None)?;
    let large_line = OrderLine::try_new("oref", "ELEGANT-LAMP", "U20")?;

    assert!(!small_batch.can_allocate(&large_line));

    Ok(())
}

#[test]
fn test_can_allocate_if_available_equal_to_required() -> Result<(), ValidationError> {
    let equal_batch = Batch::try_new("batch-001", "ELEGANT-LAMP", "U2", None)?;
    let equal_line = OrderLine::try_new("oref", "ELEGANT-LAMP", "U2")?;

    assert!(equal_batch.can_allocate(&equal_line));

    Ok(())
}

#[test]
fn test_cannot_allocate_if_skus_do_not_match() -> Result<(), ValidationError> {
    let batch = Batch::try_new("batch-001", "ELEGANT-LAMP", "U20", None)?;
    let different_line = OrderLine::try_new("oref", "EXPENSIZE-TOASTER", "U2")?;

    assert!(!batch.can_allocate(&different_line));

    Ok(())
}

#[test]
fn test_allocation_is_idempotent() -> Result<(), ValidationError> {
    let mut batch = Batch::try_new(
        "batch-001",
        "SMALL-TABLE",
        "U20",
        Some(DeliveryTime::now().to_secs()),
    )?;
    let order_line = OrderLine::try_new("oref", "SMALL-TABLE", "U2")?;

    batch.allocate(&order_line);
    batch.allocate(&order_line);

    assert_eq!(18.0, batch.available_quantity());

    Ok(())
}

#[test]
fn test_deallocate() -> Result<(), ValidationError> {
    let mut batch = Batch::try_new(
        "batch-001",
        "SMALL-TABLE",
        "U20",
        Some(DeliveryTime::now().to_secs()),
    )?;
    let order_line = OrderLine::try_new("oref", "SMALL-TABLE", "U2")?;

    assert!(batch.allocate(&order_line));
    assert!(batch.deallocate(&order_line));
    assert_eq!(20.0, batch.available_quantity());

    Ok(())
}

#[test]
fn test_can_only_deallocate_allocated_lines() -> Result<(), ValidationError> {
    let mut batch = Batch::try_new("batch-001", "SMALL-TABLE", "U20", None)?;
    let unallocated_line = OrderLine::try_new("oref", "SMALL-TABLE", "U2")?;

    assert!(!batch.deallocate(&unallocated_line));
    assert_eq!(20.0, batch.available_quantity());

    Ok(())
}
