#[cfg(test)]
mod test;

use super::{
    command::{AllocateCmd, ChangeBatchQuantityCmd, Command, CreateBatchCmd},
    error::ValidationError,
    event::{AllocatedEvent, DeallocatedEvent, Event, OutOfStockEvent},
};
use serde::{Deserialize, Serialize};
use std::str::FromStr;
use std::time::{Duration, SystemTime, UNIX_EPOCH};

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub(crate) struct UnitQuantity(u16);
impl FromStr for UnitQuantity {
    type Err = ValidationError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let input = input.parse::<u16>().map_err(|_e| ValidationError {
            msg: format!("Ivalid UnitQuantity: {input:?}"),
        })?;
        if input < 1 || input > 1000 {
            Err(ValidationError {
                msg: "UnitQuantity must be in range [1, 1000]".to_string(),
            })
        } else {
            Ok(Self(input))
        }
    }
}
impl UnitQuantity {
    pub(crate) fn value(&self) -> f32 {
        self.0 as f32
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub(crate) struct KilogramQuantity(String);
impl FromStr for KilogramQuantity {
    type Err = ValidationError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let value = input.parse::<f32>().map_err(|_e| ValidationError {
            msg: format!("Invalid KilogramQuantity: {input:?}"),
        })?;
        if value < 0.5 || value > 100.0 {
            Err(ValidationError {
                msg: "KilogramQuantity must be in range [0.5, 100.0]".to_owned(),
            })
        } else {
            Ok(Self(input.to_string()))
        }
    }
}
impl KilogramQuantity {
    pub(crate) fn value(&self) -> f32 {
        self.0.parse().expect("always valid")
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub(crate) enum Quantity {
    Unit(UnitQuantity),
    Kilogram(KilogramQuantity),
}
impl FromStr for Quantity {
    type Err = ValidationError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let (first, rest) = input.split_at(1);
        match first {
            "U" => Ok(Self::Unit(UnitQuantity::from_str(rest)?)),
            "K" => Ok(Self::Kilogram(KilogramQuantity::from_str(rest)?)),
            _ => Err(ValidationError {
                msg: "invalid Quantity".to_string(),
            }),
        }
    }
}
impl std::fmt::Display for Quantity {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Quantity::Unit(q) => write!(f, "U{:.0}", q.value()),
            Quantity::Kilogram(q) => write!(f, "K{}", q.value()),
        }
    }
}
impl Quantity {
    pub fn value(&self) -> f32 {
        match self {
            Quantity::Unit(u) => u.value(),
            Quantity::Kilogram(k) => k.value(),
        }
    }
}
//////////////////////////////////
#[allow(unused)]
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub(crate) struct Address {
    address_line1: String,
    address_line2: Option<String>,
    city: City,
    zipcode: Zipcode,
}
#[allow(unused)]
impl Address {
    fn try_new(
        address_line1: &str,
        address_line2: Option<&str>,
        city: &str,
        zipcode: &str,
    ) -> Result<Self, ValidationError> {
        Ok(Self {
            address_line1: address_line1.to_owned(),
            address_line2: address_line2.map(String::from),
            city: City::from_str(city)?,
            zipcode: Zipcode::from_str(zipcode)?,
        })
    }
}
#[allow(unused)]
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub(crate) struct City(String);
impl FromStr for City {
    type Err = ValidationError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        if input.len() < 2 || input.len() > 20 {
            Err(ValidationError {
                msg: "City len must be in range [2, 20]".to_string(),
            })
        } else {
            Ok(Self(input.to_string()))
        }
    }
}
#[allow(unused)]
impl City {
    pub(crate) fn value(&self) -> &str {
        &self.0
    }
}
#[allow(unused)]
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub(crate) struct Zipcode(String);
impl FromStr for Zipcode {
    type Err = ValidationError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        // use proper regexp
        if !input.chars().all(|c| c.is_numeric()) {
            Err(ValidationError {
                msg: format!("Zipcode must be numeric value: {input:?}"),
            })
        } else {
            Ok(Self(input.to_string()))
        }
    }
}
#[allow(unused)]
impl Zipcode {
    pub(crate) fn value(&self) -> &str {
        &self.0
    }
}
#[allow(unused)]
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub(crate) enum Currency {
    Dollar,
    Euro,
    GBP,
}
#[allow(unused)]
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub(crate) struct Money {
    currency: Currency,
    value: Cent,
}
#[allow(unused)]
impl Money {
    fn new(currency: Currency, value: Cent) -> Self {
        Self { currency, value }
    }

    pub fn value(&self) -> &Cent {
        &self.value
    }
}
#[allow(unused)]
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub(crate) struct Cent(u32);
impl TryFrom<u32> for Cent {
    type Error = ValidationError;

    fn try_from(input: u32) -> Result<Self, Self::Error> {
        if input < 1 || input > 100_000 {
            Err(ValidationError {
                msg: "Cent must be in range [1, 100_000]".to_string(),
            })
        } else {
            Ok(Self(input))
        }
    }
}
#[allow(unused)]
impl Cent {
    pub(crate) fn value(&self) -> u32 {
        self.0
    }
}
//////////////////////////////////////////////////////

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub(crate) struct OrderReference(String);
impl FromStr for OrderReference {
    type Err = ValidationError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        if input.len() < 3 || input.len() > 50 {
            Err(ValidationError {
                msg: "OrderId len must be in range [3, 50]".to_string(),
            })
        } else {
            Ok(Self(input.to_string()))
        }
    }
}
impl OrderReference {
    pub(crate) fn value(&self) -> &str {
        &self.0
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub struct ProductSku(String);
impl FromStr for ProductSku {
    type Err = ValidationError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        if input.len() < 3 || input.len() > 50 {
            Err(ValidationError {
                msg: "Sku len must be in range [3, 50]".to_string(),
            })
        } else {
            Ok(Self(input.to_string()))
        }
    }
}
impl ProductSku {
    pub fn value(&self) -> &str {
        &self.0
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub struct BatchReference(String);
impl FromStr for BatchReference {
    type Err = ValidationError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        if input.len() < 3 || input.len() > 50 {
            Err(ValidationError {
                msg: "BatchReference len must be in range [3, 50]".to_string(),
            })
        } else {
            Ok(Self(input.to_string()))
        }
    }
}
impl BatchReference {
    pub fn value(&self) -> &str {
        &self.0
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub(crate) struct OrderLine {
    pub(crate) order_id: OrderReference,
    pub(crate) sku: ProductSku,
    pub(crate) qty: Quantity,
}
impl OrderLine {
    pub(crate) fn try_new(order_id: &str, sku: &str, qty: &str) -> Result<Self, ValidationError> {
        Ok(Self {
            order_id: OrderReference::from_str(order_id)?,
            sku: ProductSku::from_str(sku)?,
            qty: Quantity::from_str(qty)?,
        })
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub struct DeliveryTime {
    // use chrono: iso datetime
    pub(crate) val: SystemTime,
}
impl TryFrom<u64> for DeliveryTime {
    type Error = ValidationError;

    fn try_from(input: u64) -> Result<Self, Self::Error> {
        let val = UNIX_EPOCH + Duration::from_secs(input);
        let now_minus_five_secs = SystemTime::now() - Duration::from_secs(5);
        if val < now_minus_five_secs {
            return Err(ValidationError {
                msg: format!("Invalid DeliveryTime: {input:?}"),
            });
        }
        Ok(Self { val })
    }
}
impl DeliveryTime {
    pub fn now() -> Self {
        Self {
            val: SystemTime::now(),
        }
    }

    pub fn to_secs(&self) -> u64 {
        self.val
            .duration_since(UNIX_EPOCH)
            .expect("duration since epoch")
            .as_secs()
    }

    pub fn plus_secs(&self, min: u64) -> Self {
        Self {
            val: self.val + Duration::from_secs(min),
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Hash, Debug, Clone)]
pub enum Message {
    Command(Command),
    Event(Event),
}

impl From<CreateBatchCmd> for Message {
    fn from(cmd: CreateBatchCmd) -> Self {
        Message::Command(Command::CreateBatch(cmd))
    }
}

impl From<AllocateCmd> for Message {
    fn from(cmd: AllocateCmd) -> Self {
        Message::Command(Command::Allocate(cmd))
    }
}

impl From<ChangeBatchQuantityCmd> for Message {
    fn from(cmd: ChangeBatchQuantityCmd) -> Self {
        Message::Command(Command::ChangeBatchQuantity(cmd))
    }
}

impl From<OutOfStockEvent> for Message {
    fn from(event: OutOfStockEvent) -> Self {
        Message::Event(Event::OutOfStock(event))
    }
}

impl From<AllocatedEvent> for Message {
    fn from(event: AllocatedEvent) -> Self {
        Message::Event(Event::Allocated(event))
    }
}

impl From<DeallocatedEvent> for Message {
    fn from(event: DeallocatedEvent) -> Self {
        Message::Event(Event::Deallocated(event))
    }
}

#[derive(PartialEq, Eq, PartialOrd, Hash, Debug, Clone, Serialize, Deserialize)]
pub struct OrderLineView {
    pub sku: String,
    pub batch_id: String,
}
