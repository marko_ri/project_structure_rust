use serde::{Deserialize, Serialize};

#[derive(PartialOrd, PartialEq, Eq, Hash, Debug, Clone, Serialize, Deserialize)]
pub enum Command {
    Allocate(AllocateCmd),
    CreateBatch(CreateBatchCmd),
    ChangeBatchQuantity(ChangeBatchQuantityCmd),
}

impl std::fmt::Display for Command {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Command::Allocate(_val) => write!(f, "Allocate"),
            Command::CreateBatch(_val) => write!(f, "CreateBatch"),
            Command::ChangeBatchQuantity(_val) => write!(f, "ChangeBatchQuantity"),
        }
    }
}

#[derive(PartialOrd, PartialEq, Eq, Hash, Debug, Clone, Serialize, Deserialize)]
pub struct AllocateCmd {
    pub(crate) order_id: String,
    pub(crate) sku: String,
    pub(crate) qty: String,
}

impl AllocateCmd {
    pub fn new(order_id: &str, sku: &str, qty: &str) -> Self {
        Self {
            order_id: order_id.to_string(),
            sku: sku.to_string(),
            qty: qty.to_string(),
        }
    }
}

#[derive(PartialOrd, PartialEq, Eq, Hash, Debug, Clone, Serialize, Deserialize)]
pub struct CreateBatchCmd {
    pub(crate) id: String,
    pub(crate) sku: String,
    pub(crate) qty: String,
    // TODO: use chrono and ISO datetime
    pub(crate) eta: Option<u64>,
}

impl CreateBatchCmd {
    pub fn new(id: &str, sku: &str, qty: &str, eta: Option<u64>) -> Self {
        Self {
            id: id.to_string(),
            sku: sku.to_string(),
            qty: qty.to_string(),
            eta,
        }
    }
}

#[derive(PartialOrd, PartialEq, Eq, Hash, Debug, Clone, Serialize, Deserialize)]
pub struct ChangeBatchQuantityCmd {
    pub(crate) batch_id: String,
    pub(crate) qty: String,
}
impl ChangeBatchQuantityCmd {
    pub fn new(batch_id: &str, qty: &str) -> Self {
        Self {
            batch_id: batch_id.to_string(),
            qty: qty.to_string(),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_create_batch_cmd_deserialization() {
        let input_without_eta = r#"
            {
              "id": "id",
              "sku": "sku",
              "qty": "qty"
            }"#;
        let input_with_eta = r#"
            {
              "id": "id",
              "sku": "sku",
              "qty": "qty",
              "eta": 123456
            }"#;

        let actual_without_eta: CreateBatchCmd = serde_json::from_str(&input_without_eta).unwrap();
        let actual_with_eta: CreateBatchCmd = serde_json::from_str(&input_with_eta).unwrap();

        let expected_without_eta = CreateBatchCmd::new("id", "sku", "qty", None);
        let expected_with_eta = CreateBatchCmd::new("id", "sku", "qty", Some(123456));

        assert_eq!(expected_without_eta, actual_without_eta);
        assert_eq!(expected_with_eta, actual_with_eta);

        assert_ne!(expected_with_eta, actual_without_eta);
        assert_ne!(expected_without_eta, actual_with_eta);
    }

    #[test]
    fn test_create_batch_cmd_serialization() {
        let expected_without_eta = r#"
            {
              "id":"id",
              "sku":"sku",
              "qty":"qty",
              "eta":null
            }"#;
        let expected_without_eta: String = expected_without_eta.lines().map(|l| l.trim()).collect();
        let expected_with_eta = r#"
            {
              "id":"id",
              "sku":"sku",
              "qty":"qty",
              "eta":123456
            }"#;
        let expected_with_eta: String = expected_with_eta.lines().map(|l| l.trim()).collect();

        let input_without_eta = CreateBatchCmd::new("id", "sku", "qty", None);
        let input_with_eta = CreateBatchCmd::new("id", "sku", "qty", Some(123456));
        let actual_without_eta = serde_json::to_string(&input_without_eta).unwrap();
        let actual_with_eta = serde_json::to_string(&input_with_eta).unwrap();

        assert_eq!(expected_without_eta, actual_without_eta);
        assert_eq!(expected_with_eta, actual_with_eta);

        assert_ne!(expected_with_eta, actual_without_eta);
        assert_ne!(expected_without_eta, actual_with_eta);
    }
}
