#[cfg(test)]
mod test_batch;
#[cfg(test)]
mod test_product;

use super::{
    command::{AllocateCmd, Command},
    error::ValidationError,
    event::{AllocatedEvent, OutOfStockEvent},
    value_object::{BatchReference, DeliveryTime, Message, OrderLine, ProductSku, Quantity},
};
use std::collections::HashSet;
use std::str::FromStr;

pub type AggregateRoot = Product;

#[derive(Debug)]
pub struct Product {
    id: ProductSku,
    batches: Vec<Batch>,
    // we can not use it for optimistic locking in sqlite
    // as there is no row level locking
    // (we can count the number of commits, and use it in tests)
    version_number: u32,
    messages: Vec<Message>,
}
entity!(Product);

impl Product {
    pub(crate) fn try_new(
        sku: &str,
        batches: Vec<Batch>,
        version_number: u32,
    ) -> Result<Self, ValidationError> {
        Ok(Self {
            id: ProductSku::from_str(sku)?,
            batches,
            version_number,
            messages: Vec::new(),
        })
    }

    pub(crate) fn allocate(&mut self, line: &OrderLine) -> Option<BatchReference> {
        self.batches.sort_unstable_by(|a, b| a.cmp_eta(b));
        if let Some(batch) = self.batches.iter_mut().find(|b| b.can_allocate(line)) {
            batch.allocate(line);
            self.messages.push(Message::from(AllocatedEvent {
                order_id: line.order_id.value().to_string(),
                sku: line.sku.value().to_string(),
                qty: line.qty.to_string(),
                batch_id: batch.id.value().to_string(),
            }));
            Some(batch.id.clone())
        } else {
            self.messages.push(Message::from(OutOfStockEvent {
                sku: line.sku.value().to_string(),
            }));
            None
        }
    }

    pub(crate) fn change_batch_quantity(&mut self, batch_id: &str, qty: Quantity) -> bool {
        let Some(batch) = self.batches.iter_mut().find(|b| b.id.value() == batch_id) else {
            return false;
        };
        batch.purchased_qty = qty;
        while batch.available_quantity() < 0.0 {
            let Some(order_line) = batch.deallocate_one() else {
                return false;
            };
            self.messages
                .push(Message::Command(Command::Allocate(AllocateCmd {
                    order_id: order_line.order_id.value().to_string(),
                    sku: order_line.sku.value().to_string(),
                    qty: order_line.qty.to_string(),
                })));
        }
        true
    }

    pub(crate) fn add_batch(&mut self, input: Batch) {
        self.batches.push(input);
    }

    pub(crate) fn batches(&self) -> &[Batch] {
        &self.batches
    }

    pub(crate) fn messages(&self) -> &[Message] {
        &self.messages
    }

    pub(crate) fn add_message(&mut self, msg: Message) {
        self.messages.push(msg);
    }

    pub(crate) fn version_number(&self) -> u32 {
        self.version_number
    }

    pub(crate) fn increment_version(&mut self) -> u32 {
        self.version_number += 1;
        self.version_number
    }

    pub(crate) fn id(&self) -> &ProductSku {
        &self.id
    }
}

#[derive(Debug)]
pub(crate) struct Batch {
    pub(crate) id: BatchReference,
    pub(crate) sku: ProductSku,
    pub(crate) eta: Option<DeliveryTime>,
    pub(crate) purchased_qty: Quantity,
    allocations: HashSet<OrderLine>,
}
entity!(Batch);

impl Batch {
    pub(crate) fn try_new(
        id: &str,
        sku: &str,
        qty: &str,
        eta: Option<u64>,
    ) -> Result<Self, ValidationError> {
        let eta = match eta {
            Some(e) => Some(DeliveryTime::try_from(e)?),
            None => None,
        };
        Ok(Self {
            id: BatchReference::from_str(id)?,
            sku: ProductSku::from_str(sku)?,
            eta,
            purchased_qty: Quantity::from_str(qty)?,
            allocations: HashSet::new(),
        })
    }

    pub(crate) fn allocate(&mut self, line: &OrderLine) -> bool {
        if self.can_allocate(line) {
            self.allocations.insert(line.clone());
            true
        } else {
            false
        }
    }

    pub(crate) fn deallocate(&mut self, line: &OrderLine) -> bool {
        self.allocations.remove(line)
    }

    pub(crate) fn deallocate_one(&mut self) -> Option<OrderLine> {
        if let Some(rand_order_line) = self.allocations.iter().next() {
            let order_line = rand_order_line.clone();
            self.allocations.remove(&order_line);
            return Some(order_line);
        };
        None
    }

    pub(crate) fn allocated_quantity(&self) -> f32 {
        self.allocations.iter().map(|a| a.qty.value()).sum()
    }

    pub(crate) fn available_quantity(&self) -> f32 {
        self.purchased_qty.value() - self.allocated_quantity()
    }

    pub(crate) fn can_allocate(&self, line: &OrderLine) -> bool {
        self.sku == line.sku && self.available_quantity() >= line.qty.value()
    }

    pub(crate) fn cmp_eta(&self, other: &Batch) -> std::cmp::Ordering {
        if self.eta.is_none() {
            return std::cmp::Ordering::Less;
        }
        if other.eta.is_none() {
            return std::cmp::Ordering::Greater;
        }
        self.eta.as_ref().unwrap().cmp(other.eta.as_ref().unwrap())
    }

    pub(crate) fn allocations(&self) -> &HashSet<OrderLine> {
        &self.allocations
    }

    pub(crate) fn add_allocation(&mut self, order_line: OrderLine) {
        self.allocations.insert(order_line);
    }
}
