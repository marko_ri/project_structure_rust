use std::convert::From;
use std::error::Error;

use crate::adapter::port::PubSubContent;

pub type GenericResult<T> = Result<T, Box<dyn Error + Send + Sync>>;

#[derive(Debug)]
pub struct ValidationError {
    pub msg: String,
}
impl ValidationError {
    pub fn new(msg: String) -> Self {
        Self { msg }
    }
}
impl std::fmt::Display for ValidationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "ValidationError: {}", self.msg)
    }
}
impl Error for ValidationError {}

#[derive(Debug, PartialEq)]
pub struct OutOfStockError {
    msg: String,
}
impl OutOfStockError {
    pub fn new(msg: String) -> Self {
        Self { msg }
    }
}
impl std::fmt::Display for OutOfStockError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "OutOfStockError: {}", self.msg)
    }
}
impl Error for OutOfStockError {}

#[derive(Debug)]
pub struct MessageBusError {
    msg: String,
}
impl MessageBusError {
    pub fn new(msg: String) -> Self {
        Self { msg }
    }
}
impl std::fmt::Display for MessageBusError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "DbError: {}", self.msg)
    }
}
impl Error for MessageBusError {}
impl From<std::sync::mpsc::SendError<PubSubContent>> for MessageBusError {
    fn from(err: std::sync::mpsc::SendError<PubSubContent>) -> Self {
        Self {
            msg: err.to_string(),
        }
    }
}
impl From<std::sync::mpsc::TryRecvError> for MessageBusError {
    fn from(err: std::sync::mpsc::TryRecvError) -> Self {
        Self {
            msg: err.to_string(),
        }
    }
}
impl From<std::sync::mpsc::RecvTimeoutError> for MessageBusError {
    fn from(err: std::sync::mpsc::RecvTimeoutError) -> Self {
        Self {
            msg: err.to_string(),
        }
    }
}

#[derive(Debug)]
pub struct DbError {
    msg: String,
}
impl DbError {
    pub fn new(msg: String) -> Self {
        Self { msg }
    }
}
impl std::fmt::Display for DbError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "DbError: {}", self.msg)
    }
}
impl Error for DbError {}

impl From<sqlx::Error> for DbError {
    fn from(err: sqlx::Error) -> Self {
        Self {
            msg: err.to_string(),
        }
    }
}

#[derive(Debug)]
pub enum AllocationError {
    Validation(ValidationError),
    OutOfStock(OutOfStockError),
    Db(DbError),
    MessageBus(MessageBusError),
}
impl std::fmt::Display for AllocationError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match *self {
            AllocationError::Validation(ref err) => write!(f, "{}", err),
            AllocationError::OutOfStock(ref err) => write!(f, "{}", err),
            AllocationError::Db(ref err) => write!(f, "{}", err),
            AllocationError::MessageBus(ref err) => write!(f, "{}", err),
        }
    }
}

impl Error for AllocationError {}

impl AllocationError {
    pub fn message(&self) -> &str {
        match self {
            AllocationError::Validation(e) => &e.msg,
            AllocationError::OutOfStock(e) => &e.msg,
            AllocationError::Db(e) => &e.msg,
            AllocationError::MessageBus(e) => &e.msg,
        }
    }
}

impl From<ValidationError> for AllocationError {
    fn from(err: ValidationError) -> AllocationError {
        AllocationError::Validation(err)
    }
}

impl From<OutOfStockError> for AllocationError {
    fn from(err: OutOfStockError) -> AllocationError {
        AllocationError::OutOfStock(err)
    }
}

impl From<DbError> for AllocationError {
    fn from(err: DbError) -> AllocationError {
        AllocationError::Db(err)
    }
}

impl From<MessageBusError> for AllocationError {
    fn from(err: MessageBusError) -> AllocationError {
        AllocationError::MessageBus(err)
    }
}
