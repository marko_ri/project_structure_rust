use super::*;
use crate::domain::error::ValidationError;
use std::str::FromStr;

#[test]
fn test_unit_quantity_validation() {
    assert_eq!(UnitQuantity::from_str("15").unwrap().value(), 15.0);
    assert!(UnitQuantity::from_str("1001").is_err());
}

#[test]
fn test_kilogram_quantity_validation() {
    assert_eq!(KilogramQuantity::from_str("0.6").unwrap().value(), 0.6);
    assert!(KilogramQuantity::from_str("101.01").is_err());
}

#[test]
fn test_city_validation() {
    assert_eq!(City::from_str("abc").unwrap().value(), "abc");
    assert!(City::from_str("a").is_err());
}

#[test]
fn test_zipcode_validation() {
    assert_eq!(Zipcode::from_str("123").unwrap().value(), "123");
    assert!(Zipcode::from_str("a12").is_err());
}

#[test]
fn test_address_validation() {
    assert!(Address::try_new("street1", None, "city", "123",).is_ok());
    assert!(Address::try_new("street1", None, "city", "123a",).is_err());
}

#[test]
fn test_billing_amount_validation() {
    let currency = Currency::Euro;
    let value = Cent::try_from(1_000).unwrap();
    assert_eq!(Money::new(currency, value).value(), &Cent(1_000));
}

#[test]
fn test_price_validation() {
    assert_eq!(Cent::try_from(1_000).unwrap().value(), 1_000);
    assert!(Cent::try_from(100_001).is_err());
}

#[test]
fn test_order_line_equality() -> Result<(), ValidationError> {
    let order_line1 = OrderLine::try_new("oref", "TABLE", "U2")?;
    let order_line2 = OrderLine::try_new("oref", "TABLE", "U2")?;
    assert_eq!(order_line1, order_line2);

    let order_line3 = OrderLine::try_new("oref", "TABLE", "U3")?;
    assert_ne!(order_line1, order_line3);

    let order_line4 = OrderLine::try_new("different-oref", "TABLE", "U2")?;
    assert_ne!(order_line1, order_line4);

    let order_line5 = OrderLine::try_new("oref", "DIFFERENT=TABLE", "U2")?;
    assert_ne!(order_line1, order_line5);

    Ok(())
}
