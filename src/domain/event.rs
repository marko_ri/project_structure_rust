use serde::{Deserialize, Serialize};

#[derive(PartialOrd, PartialEq, Eq, Hash, Debug, Clone)]
pub enum Event {
    OutOfStock(OutOfStockEvent),
    Allocated(AllocatedEvent),
    Deallocated(DeallocatedEvent),
}

impl std::fmt::Display for Event {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Event::OutOfStock(_val) => write!(f, "OutOfStock"),
            Event::Allocated(_val) => write!(f, "Allocated"),
            Event::Deallocated(_val) => write!(f, "Deallocated"),
        }
    }
}

#[derive(PartialOrd, PartialEq, Eq, Hash, Debug, Clone, Serialize, Deserialize)]
pub struct OutOfStockEvent {
    pub sku: String,
}

#[derive(PartialOrd, PartialEq, Eq, Hash, Debug, Clone, Serialize, Deserialize)]
pub struct AllocatedEvent {
    pub order_id: String,
    pub sku: String,
    pub qty: String,
    pub batch_id: String,
}

#[derive(PartialOrd, PartialEq, Eq, Hash, Debug, Clone, Serialize, Deserialize)]
pub struct DeallocatedEvent {
    pub order_id: String,
    pub sku: String,
    pub qty: String,
}
