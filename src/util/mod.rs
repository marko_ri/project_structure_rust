#[macro_use]
pub(crate) mod macros;
pub(crate) mod db_wrapper;

use log::error;

use crate::domain::error::GenericResult;
use std::any::Any;
use std::collections::HashMap;
use std::fs;
use std::path::Path;
use std::time::{Duration, SystemTime, UNIX_EPOCH};

#[allow(unused)]
fn is_type<T: 'static>(x: &dyn Any) -> bool {
    x.is::<T>()
}

#[allow(unused)]
fn random_seed() -> u64 {
    std::hash::Hasher::finish(&std::hash::BuildHasher::build_hasher(
        &std::collections::hash_map::RandomState::new(),
    ))
}

#[allow(unused)]
fn unique_i64() -> i64 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .subsec_nanos()
        .into()
}

pub fn retry<F, O>(num_retries: u8, sleep_ms: u64, f: F) -> GenericResult<O>
where
    F: Fn() -> GenericResult<O>,
{
    let mut err = None;
    for i in 0..num_retries {
        match f() {
            Ok(res) => {
                return Ok(res);
            }
            Err(e) => {
                error!("Call FAILED on retry: {}/{num_retries} with error: {e:?}", i + 1);
                std::thread::sleep(Duration::from_millis(sleep_ms * (i as u64 + 1)));
                err = Some(e);
            }
        }
    }
    Err(err.unwrap())
}

pub(crate) fn read_env_vars(file_name: &str) -> GenericResult<()> {
    let contents = fs::read_to_string(Path::new(file_name))?;
    let key_val = parse_env_file(contents)?;
    for (key, val) in key_val.iter() {
        std::env::set_var(key, val);
    }
    Ok(())
}

fn parse_env_file(input: String) -> GenericResult<HashMap<String, String>> {
    let mut result = HashMap::new();
    for line in input.lines() {
        if let Some((key, val)) = line.split_once('=') {
            result.insert(key.to_string(), val.to_string());
        } else {
            return Err("Invalid entry, expected (KEY=VALUE)".into());
        }
    }
    Ok(result)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_random() {
        let r1 = random_seed();
        let r2 = random_seed();
        assert_ne!(r1, r2);
    }

    #[test]
    fn test_unique() {
        assert_ne!(unique_i64(), unique_i64());
    }

    #[test]
    fn test_type() {
        let x = "abc".to_string();
        assert!(is_type::<String>(&x));
    }

    #[test]
    fn test_retry() {
        use crate::domain::error::ValidationError;
        let num_retries = 3;
        let sleep_ms = 1;

        fn success() -> GenericResult<String> {
            Ok("success".into())
        }
        let result = retry(num_retries, sleep_ms, success);
        assert_eq!("success", result.unwrap());

        fn fail() -> GenericResult<String> {
            Err(ValidationError::new("this function will always fail".to_string()).into())
        }
        let result = retry(num_retries, sleep_ms, fail);
        assert_eq!(
            "ValidationError: this function will always fail",
            result.err().unwrap().to_string()
        );
    }

    #[test]
    fn test_read_env_vars() -> GenericResult<()> {
        let path = std::env::current_dir()?;
        println!("The current directory is {}", path.display());
        read_env_vars(".env")?;
        assert_eq!(std::env::var("DATABASE_URL")?, "sqlite:allocations.db");
        assert_eq!(std::env::var("DATABASE_URL_TEST")?, "sqlite::memory:");

        Ok(())
    }
}
