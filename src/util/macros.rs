macro_rules! entity {
    ($($t:ty),+) => ($(
        impl std::cmp::PartialEq for $t {
            fn eq(&self, other: &Self) -> bool {
                self.id.value() == other.id.value()
            }
        }

        impl std::cmp::Eq for $t {}

        impl std::cmp::Ord for $t {
            fn cmp(&self, other: &Self) -> std::cmp::Ordering {
                self.id.value().cmp(&other.id.value())
            }
        }

        impl std::cmp::PartialOrd for $t {
            fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
                Some(self.cmp(other))
            }
        }

        impl std::hash::Hash for $t {
           fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
               self.id.value().hash(state);
           }
        }
    )+)
}
