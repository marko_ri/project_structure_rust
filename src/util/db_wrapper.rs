use sqlx::sqlite::{SqliteConnectOptions, SqliteJournalMode, SqlitePoolOptions, SqliteSynchronous};
use std::str::FromStr;
use tokio::runtime::Runtime;

// maybe sqlx was not the perfect choice,
// but it was a nice intro to async rust (before this wrapper was made :))
pub(crate) struct SqlxSync {
    pub(crate) pool: sqlx::SqlitePool,
    pub(crate) rt: Runtime,
}

type Query<'a> = sqlx::query::Query<'a, sqlx::sqlite::Sqlite, sqlx::sqlite::SqliteArguments<'a>>;

impl SqlxSync {
    pub(crate) fn connect(db_url: &str) -> Result<Self, sqlx::Error> {
        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()?;
        let connection_options = SqliteConnectOptions::from_str(db_url)?
            .create_if_missing(true)
            //.busy_timeout(std::time::Duration::from_secs(10))
            .foreign_keys(true)
            .journal_mode(SqliteJournalMode::Wal)
            .synchronous(SqliteSynchronous::Normal);
        let pool = rt.block_on(
            SqlitePoolOptions::new()
                //.max_connections(1)
                .connect_with(connection_options),
        )?;
        Ok(Self { pool, rt })
    }

    pub(crate) fn migrate(&self) -> Result<(), sqlx::migrate::MigrateError> {
        self.rt
            .block_on(sqlx::migrate!("./migrations").run(&self.pool))
    }

    pub(crate) fn start_tx(&self) -> Result<sqlx::Transaction<'static, sqlx::Sqlite>, sqlx::Error> {
        self.rt.block_on(self.pool.begin())
    }

    pub(crate) fn commit_tx(
        &self,
        tx: sqlx::Transaction<'static, sqlx::Sqlite>,
    ) -> Result<(), sqlx::Error> {
        self.rt.block_on(tx.commit())
    }

    pub(crate) fn fetch_all<'a>(
        &self,
        executor: impl sqlx::sqlite::SqliteExecutor<'a>,
        query: Query,
    ) -> Result<Vec<sqlx::sqlite::SqliteRow>, sqlx::Error> {
        self.rt.block_on(query.fetch_all(executor))
    }

    pub(crate) fn fetch_optional<'a>(
        &self,
        executor: impl sqlx::sqlite::SqliteExecutor<'a>,
        query: Query,
    ) -> Result<Option<sqlx::sqlite::SqliteRow>, sqlx::Error> {
        self.rt.block_on(query.fetch_optional(executor))
    }

    pub(crate) fn execute<'a>(
        &self,
        executor: impl sqlx::sqlite::SqliteExecutor<'a>,
        query: Query,
    ) -> Result<sqlx::sqlite::SqliteQueryResult, sqlx::Error> {
        self.rt.block_on(query.execute(executor))
    }
}
