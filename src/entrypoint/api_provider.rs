#[cfg(test)]
mod test;

use crate::{
    adapter::port::{ExposedApi, PubSub, PubSubContent, PubSubTopic, Subscription},
    bootstrap::Bootstrap,
    domain::{
        command::{AllocateCmd, ChangeBatchQuantityCmd, CreateBatchCmd},
        error::MessageBusError,
        value_object::Message,
    },
    util,
};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub struct PoorManApiProvider<'a> {
    bootstrap: &'a Bootstrap,
    subscriptions: HashMap<PubSubTopic, Box<dyn Subscription>>,
}

impl<'a> PoorManApiProvider<'a> {
    pub fn new(bootstrap: &'a Bootstrap) -> Self {
        Self {
            bootstrap,
            subscriptions: HashMap::new(),
        }
    }

    pub fn subscribe(&mut self, topic: &PubSubTopic, pub_sub: &dyn PubSub) {
        let subscription = pub_sub.subscribe(topic);
        self.subscriptions.insert(topic.clone(), subscription);
    }

    pub fn subscription(&self, topic: &PubSubTopic) -> Option<&dyn Subscription> {
        self.subscriptions.get(topic).map(|s| s.as_ref())
    }

    pub fn retrieve_from_channel(
        &self,
        topic: &PubSubTopic,
    ) -> Result<PubSubContent, MessageBusError> {
        let Some(subscription) = self.subscriptions.get(topic) else {
            return Err(MessageBusError::new(format!("You are not subscribed to: {:?}", topic)));
        };
        // if there is nothing on the channel, try multiple times...
        let num_retries = 3;
        let sleep_ms = 1;
        util::retry(num_retries, sleep_ms, || {
            subscription.get_message().map_err(|e| e.into())
        })
        .map_err(|e| MessageBusError::new(e.to_string()))
    }
}

#[derive(Serialize, Deserialize)]
pub struct ApiResponse {
    pub message: String,
    pub status_code: u16,
}
impl ApiResponse {
    fn new(msg: &str, status_code: u16) -> Self {
        Self {
            message: msg.to_string(),
            status_code,
        }
    }
    fn ok() -> Self {
        Self {
            message: "OK".to_string(),
            status_code: 200,
        }
    }
    fn err(msg: &str) -> Self {
        Self {
            message: msg.to_string(),
            status_code: 500,
        }
    }
    fn serialize(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }
}

// TODO: prefer web server :)
impl<'a> ExposedApi for PoorManApiProvider<'a> {
    fn add_batch(&self, json_in: &str) -> String {
        let cmd = match serde_json::from_str::<CreateBatchCmd>(json_in) {
            Ok(val) => val,
            Err(_e) => return ApiResponse::err("Invalid input").serialize(),
        };
        match self
            .bootstrap
            .bus()
            .publish(Message::from(cmd), self.bootstrap.repo())
        {
            Ok(_val) => ApiResponse::ok().serialize(),
            Err(_e) => ApiResponse::err("Internal Error").serialize(),
        }
    }

    fn allocate(&self, json_in: &str) -> String {
        let cmd = match serde_json::from_str::<AllocateCmd>(json_in) {
            Ok(val) => val,
            Err(_e) => return ApiResponse::err("Invalid input").serialize(),
        };
        match self
            .bootstrap
            .bus()
            .publish(Message::from(cmd), self.bootstrap.repo())
        {
            Ok(_val) => ApiResponse::ok().serialize(),
            Err(e) => ApiResponse::new(&e.to_string(), 400).serialize(),
        }
    }

    fn handle_change_batch_quantity(&self) -> String {
        let topic = PubSubTopic::ChangeBatchQuantity;
        let res = match self.retrieve_from_channel(&topic) {
            Ok(m) => {
                let cmd = match serde_json::from_str::<ChangeBatchQuantityCmd>(&m) {
                    Ok(val) => val,
                    Err(_e) => return ApiResponse::err("Invalid input on pub-sub").serialize(),
                };
                match self
                    .bootstrap
                    .bus()
                    .publish(Message::from(cmd), self.bootstrap.repo())
                {
                    Ok(_val) => ApiResponse::ok(),
                    Err(_e) => ApiResponse::err("Internal error"),
                }
            }
            Err(_e) => ApiResponse::err(&format!("No message for topic: {:?}", topic)),
        };

        res.serialize()
    }

    fn get_allocation(&self, order_id: &str) -> String {
        // direct db access.. for view purposes
        match self
            .bootstrap
            .repo()
            .orderlineview_fetch_by_order_id(order_id)
        {
            Ok(views) => {
                if views.is_empty() {
                    ApiResponse::new("Not Found", 404).serialize()
                } else {
                    let message = serde_json::to_string(&views).unwrap();
                    ApiResponse::new(&message, 200).serialize()
                }
            }
            Err(_e) => ApiResponse::new("Not Found", 404).serialize(),
        }
    }
}
