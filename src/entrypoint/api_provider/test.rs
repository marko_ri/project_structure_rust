use crate::{
    adapter::port::{ExposedApi, Repository},
    bootstrap::Bootstrap,
    domain::{
        command::{AllocateCmd, CreateBatchCmd},
        error::{AllocationError, GenericResult},
        value_object::{DeliveryTime, OrderLineView, ProductSku},
    },
    entrypoint::api_provider::{ApiResponse, PoorManApiProvider},
};
use once_cell::sync::OnceCell;
use std::str::FromStr;

static BOOTSTRAP: OnceCell<Bootstrap> = OnceCell::new();

fn bootstrap() -> Bootstrap {
    // because tests run in parallel and we are sharing the same sqlite connection, run these in sequence.
    // (if not-in-memory db is used, run tests with: cargo test -- --test-threads=1)
    // "Note that the journal_mode for an in-memory database is either MEMORY or OFF and can not be changed to a different value."
    Bootstrap::new("DATABASE_URL_TEST")
}

fn cleanup(repo: &dyn Repository, skus: &[&str]) -> Result<(), AllocationError> {
    for sku in skus {
        let rows_affected = repo.delete(None, &ProductSku::from_str(sku)?)?;
        assert_eq!(1, rows_affected);
    }
    Ok(())
}

#[test]
fn test_api_provider_sequentially() -> GenericResult<()> {
    let bootstrap = BOOTSTRAP.get_or_init(bootstrap);
    let api_provider = PoorManApiProvider::new(bootstrap);

    test_happy_path_returns_202_and_batch_is_allocated(bootstrap.repo(), &api_provider)?;
    test_unhappy_path_returns_400_and_error_message(&api_provider)?;

    Ok(())
}

fn test_happy_path_returns_202_and_batch_is_allocated(
    repo: &dyn Repository,
    api_provider: &PoorManApiProvider,
) -> GenericResult<()> {
    let (sku1, sku2) = ("CHAIR-RED", "CHAIR-BLUE");
    let now = DeliveryTime::now();
    let later = now.plus_secs(60);

    let cmd_batch_early = CreateBatchCmd::new("early", sku1, "U100", Some(now.to_secs()));
    let cmd_batch_later = CreateBatchCmd::new("later", sku1, "U100", Some(later.to_secs()));
    let cmd_batch_other = CreateBatchCmd::new("other", sku2, "U100", None);
    api_provider.add_batch(&serde_json::to_string(&cmd_batch_early)?);
    api_provider.add_batch(&serde_json::to_string(&cmd_batch_later)?);
    api_provider.add_batch(&serde_json::to_string(&cmd_batch_other)?);

    let cmd_allocate = AllocateCmd::new("order_id", sku1, "U3");
    let res = api_provider.allocate(&serde_json::to_string(&cmd_allocate)?);
    let res: ApiResponse = serde_json::from_str(&res)?;
    assert_eq!("OK", res.message);

    let res = api_provider.get_allocation("order_id");
    let res: ApiResponse = serde_json::from_str(&res)?;
    assert_eq!(200, res.status_code);
    let views: Vec<OrderLineView> = serde_json::from_str(&res.message)?;
    assert_eq!(sku1, views[0].sku);
    assert_eq!("early", views[0].batch_id);

    cleanup(repo, &[sku1, sku2]).map_err(|e| e.to_string().into())
}

fn test_unhappy_path_returns_400_and_error_message(
    api_provider: &PoorManApiProvider,
) -> GenericResult<()> {
    let cmd_allocate = AllocateCmd::new("order_id", "noexisting-sku", "U3");
    let res = api_provider.allocate(&serde_json::to_string(&cmd_allocate)?);
    let res: ApiResponse = serde_json::from_str(&res)?;
    assert_eq!(400, res.status_code);

    let res = api_provider.get_allocation("order_id");
    let res: ApiResponse = serde_json::from_str(&res)?;
    assert_eq!(404, res.status_code);

    Ok(())
}
