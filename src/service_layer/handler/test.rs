use crate::{
    adapter::{
        email::Email,
        port::{Notification, Repository},
    },
    bootstrap::Bootstrap,
    domain::{
        command::{AllocateCmd, ChangeBatchQuantityCmd, CreateBatchCmd},
        error::{AllocationError, GenericResult},
        value_object::{DeliveryTime, Message, ProductSku},
    },
    util,
};
use log::{error, info};
use once_cell::sync::OnceCell;
use std::str::FromStr;

static NOTIFY: OnceCell<Box<dyn Notification>> = OnceCell::new();
static BOOTSTRAP: OnceCell<Bootstrap> = OnceCell::new();

fn bootstrap() -> Bootstrap {
    let nofify = NOTIFY.get_or_init(|| Box::new(Email::new()));
    // because tests run in parallel and we are sharing the same sqlite connection, run these in sequence.
    // (if not-in-memory db is used, run tests with: cargo test -- --test-threads=1)
    // "Note that the journal_mode for an in-memory database is either MEMORY or OFF and can not be changed to a different value."
    Bootstrap::new_with_notification("DATABASE_URL_TEST", nofify.as_ref())
}

fn cleanup(repo: &dyn Repository, sku: &str) -> Result<(), AllocationError> {
    let rows_affected = repo.delete(None, &ProductSku::from_str(sku)?)?;
    assert_eq!(1, rows_affected);

    Ok(())
}

#[test]
fn test_handler_sequentially() -> Result<(), AllocationError> {
    let bootstrap = BOOTSTRAP.get_or_init(bootstrap);

    test_add_batch_for_new_product(&bootstrap)?;
    test_add_batch_for_existing_product(&bootstrap)?;
    test_allocate_returns_allocation(&bootstrap)?;
    test_allocate_errors_for_invalid_sku(&bootstrap)?;
    test_sends_email_on_out_of_stock_error(&bootstrap)?;
    test_changes_available_quantity(&bootstrap)?;
    test_reallocates_if_necessary(&bootstrap)?;
    test_rolls_back_on_error(&bootstrap)?;

    //// if we enable these tests, we will get "database blocked"
    //// as they are creating extra threads that try to access db
    // test_concurrent_updates_to_db_are_not_allowed(&bootstrap)?;
    // test_concurrent_updates_tried_multiple_times_do_succeed(&bootstrap)?;

    Ok(())
}

fn test_add_batch_for_new_product(bootstrap: &Bootstrap) -> Result<(), AllocationError> {
    let sku = "CRUNCHY-ARMCHAIR";

    bootstrap.bus().publish(
        Message::from(CreateBatchCmd::new("a123", sku, "U100", None)),
        &*bootstrap.repo(),
    )?;

    let db_product = bootstrap
        .repo()
        .fetch_by_id(None, &ProductSku::from_str(sku)?)?;
    assert_eq!(1, db_product.batches().len());

    cleanup(bootstrap.repo(), sku)
}

fn test_add_batch_for_existing_product(bootstrap: &Bootstrap) -> Result<(), AllocationError> {
    let sku = "GARISH-RUG";

    bootstrap.bus().publish(
        Message::from(CreateBatchCmd::new("b123", sku, "U100", None)),
        &*bootstrap.repo(),
    )?;
    bootstrap.bus().publish(
        Message::from(CreateBatchCmd::new("b1234", sku, "U99", None)),
        &*bootstrap.repo(),
    )?;

    let db_product = bootstrap
        .repo()
        .fetch_by_id(None, &ProductSku::from_str(sku)?)?;
    assert_eq!(2, db_product.batches().len());
    assert_eq!(2, db_product.version_number());

    cleanup(bootstrap.repo(), sku)
}

fn test_allocate_returns_allocation(bootstrap: &Bootstrap) -> Result<(), AllocationError> {
    let sku = "COMPLICATED-LAMP";

    bootstrap.bus().publish(
        Message::from(CreateBatchCmd::new("c123", sku, "U100", None)),
        &*bootstrap.repo(),
    )?;

    bootstrap.bus().publish(
        Message::from(AllocateCmd::new("oref", sku, "U10")),
        &*bootstrap.repo(),
    )?;

    // test saved value
    let db_product = bootstrap
        .repo()
        .fetch_by_id(None, &ProductSku::from_str(sku)?)?;
    assert_eq!(2, db_product.version_number());

    let batches = db_product.batches();
    assert_eq!(1, batches.len());
    assert_eq!(90.0, batches[0].available_quantity());
    let db_order_lines = batches[0].allocations();
    assert_eq!(1, db_order_lines.len());
    assert_eq!(
        "oref",
        db_order_lines.iter().next().unwrap().order_id.value()
    );

    cleanup(bootstrap.repo(), sku)
}

fn test_allocate_errors_for_invalid_sku(bootstrap: &Bootstrap) -> Result<(), AllocationError> {
    bootstrap.bus().publish(
        Message::from(CreateBatchCmd::new("d123", "EXISTENT-SKU", "U100", None)),
        &*bootstrap.repo(),
    )?;

    let result = bootstrap.bus().publish(
        Message::from(AllocateCmd::new("oref", "NONEXISTENT-SKU", "U10")),
        &*bootstrap.repo(),
    );

    assert!(result.is_err());
    assert_eq!(
        "No batch with sku: ProductSku(\"NONEXISTENT-SKU\")",
        result.err().unwrap().message(),
    );

    cleanup(bootstrap.repo(), "EXISTENT-SKU")
}

fn test_sends_email_on_out_of_stock_error(bootstrap: &Bootstrap) -> Result<(), AllocationError> {
    let notify = NOTIFY.get_or_init(|| Box::new(Email::new()));
    let sku = "POPULAR-CURTAINS";

    bootstrap.bus().publish(
        Message::from(CreateBatchCmd::new("e123", sku, "U9", None)),
        &*bootstrap.repo(),
    )?;

    bootstrap.bus().publish(
        Message::from(AllocateCmd::new("oref", sku, "U10")),
        &*bootstrap.repo(),
    )?;

    assert!(notify.is_address_cached("some_address"));

    cleanup(bootstrap.repo(), sku)
}

fn test_changes_available_quantity(bootstrap: &Bootstrap) -> Result<(), AllocationError> {
    let sku = "ADORABLE-SETTEE";

    bootstrap.bus().publish(
        Message::from(CreateBatchCmd::new("f123", sku, "U100", None)),
        &*bootstrap.repo(),
    )?;

    let db_product = bootstrap
        .repo()
        .fetch_by_id(None, &ProductSku::from_str(sku)?)?;
    assert_eq!(
        100.0,
        db_product.batches().first().unwrap().available_quantity()
    );

    bootstrap.bus().publish(
        Message::from(ChangeBatchQuantityCmd {
            batch_id: "f123".to_string(),
            qty: "U50".to_string(),
        }),
        &*bootstrap.repo(),
    )?;

    let db_product = bootstrap
        .repo()
        .fetch_by_id(None, &ProductSku::from_str(sku)?)?;
    assert_eq!(
        50.0,
        db_product.batches().first().unwrap().available_quantity()
    );

    cleanup(bootstrap.repo(), sku)
}

fn test_reallocates_if_necessary(bootstrap: &Bootstrap) -> Result<(), AllocationError> {
    let sku = "INDIFFERENT-TABLE";
    let today = DeliveryTime::now().plus_secs(60);

    let event_history = [
        Message::from(CreateBatchCmd::new("g123", sku, "U50", None)),
        Message::from(CreateBatchCmd::new(
            "h123",
            sku,
            "U50",
            Some(today.to_secs()),
        )),
        Message::from(AllocateCmd::new("order1", sku, "U20")),
        Message::from(AllocateCmd::new("order2", sku, "U20")),
    ];

    for e in event_history.into_iter() {
        bootstrap.bus().publish(e, &*bootstrap.repo())?;
    }

    let db_product = bootstrap
        .repo()
        .fetch_by_id(None, &ProductSku::from_str(sku)?)?;
    let batches = db_product.batches();
    let batch1 = batches.iter().find(|b| b.id.value() == "g123").unwrap();
    let batch2 = batches.iter().find(|b| b.id.value() == "h123").unwrap();

    assert_eq!(10.0, batch1.available_quantity());
    assert_eq!(50.0, batch2.available_quantity());

    bootstrap.bus().publish(
        Message::from(ChangeBatchQuantityCmd {
            batch_id: "g123".to_string(),
            qty: "U25".to_string(),
        }),
        &*bootstrap.repo(),
    )?;
    let db_product = bootstrap
        .repo()
        .fetch_by_id(None, &ProductSku::from_str(sku)?)?;
    let batches = db_product.batches();
    let batch1 = batches.iter().find(|b| b.id.value() == "g123").unwrap();
    let batch2 = batches.iter().find(|b| b.id.value() == "h123").unwrap();

    // order1 or order2 will be deallocated, so we'll have 25 - 20
    assert_eq!(5.0, batch1.available_quantity());
    // and 20 will be reallocated to the next batch
    assert_eq!(30.0, batch2.available_quantity());

    cleanup(bootstrap.repo(), sku)
}

fn test_rolls_back_on_error(bootstrap: &Bootstrap) -> Result<(), AllocationError> {
    let invalid_batch_id = "b";

    let result = bootstrap.bus().publish(
        Message::from(CreateBatchCmd::new(
            invalid_batch_id,
            "REAL-SKU",
            "U1",
            None,
        )),
        &*bootstrap.repo(),
    );

    assert!(result.is_err());

    let result_fetch_product = bootstrap
        .repo()
        .fetch_by_id(None, &ProductSku::from_str("REAL-SKU")?);
    assert!(result_fetch_product.is_err());

    Ok(())
}

fn try_to_allocate_once(
    batch_id: &str,
    sku: &str,
    bootstrap: &Bootstrap,
) -> Result<(), AllocationError> {
    let thread_id = std::thread::current().id();
    info!("Start thread {thread_id:?}: {batch_id}-{sku}");

    bootstrap.bus().publish(
        Message::from(CreateBatchCmd::new(batch_id, sku, "U100", None)),
        &*bootstrap.repo(),
    )?;

    bootstrap.bus().publish(
        Message::from(AllocateCmd::new("oref", sku, "U10")),
        &*bootstrap.repo(),
    )?;

    info!("End thread {thread_id:?}: {batch_id}-{sku}");
    Ok(())
}

// sqlite will not allow concurrent writes: error is "database is locked"
// https://www.sqlite.org/isolation.html
fn test_concurrent_updates_to_db_are_not_allowed(
    bootstrap: &'static Bootstrap,
) -> Result<(), AllocationError> {
    let sku = "CONCURRENT_CHAIR";
    let handle1 = std::thread::spawn(|| -> Result<(), AllocationError> {
        try_to_allocate_once("c123", sku, bootstrap)
    });
    let handle2 = std::thread::spawn(|| -> Result<(), AllocationError> {
        try_to_allocate_once("d123", sku, bootstrap)
    });

    let result = [handle1.join().unwrap(), handle2.join().unwrap()];
    assert!(result.iter().any(|r| r.is_err()));
    error!("ERROR: {:?}", result.iter().find(|h| h.is_err()));

    let db_product = bootstrap
        .repo()
        .fetch_by_id(None, &ProductSku::from_str(sku)?)?;
    assert_eq!(2, db_product.version_number());

    cleanup(bootstrap.repo(), sku)
}

fn try_to_allocate_multiple_times(
    batch_id: &str,
    sku: &str,
    bootstrap: &Bootstrap,
) -> GenericResult<()> {
    let num_retries = 3;
    let sleep_ms = 1;
    let thread_id = std::thread::current().id();
    info!("Start thread {thread_id:?}: {batch_id}-{sku}");

    let create_batch = || {
        bootstrap
            .bus()
            .publish(
                Message::from(CreateBatchCmd::new(batch_id, sku, "U100", None)),
                &*bootstrap.repo(),
            )
            .map_err(|e| e.into())
    };
    if let Err(e) = util::retry(num_retries, sleep_ms, create_batch) {
        return Err(e);
    }

    let allocate = || {
        bootstrap
            .bus()
            .publish(
                Message::from(AllocateCmd::new("oref", sku, "U10")),
                &*bootstrap.repo(),
            )
            .map_err(|e| e.into())
    };
    if let Err(e) = util::retry(num_retries, sleep_ms, allocate) {
        return Err(e);
    }

    info!("End thread {thread_id:?}: {batch_id}-{sku}");

    Ok(())
}

fn test_concurrent_updates_tried_multiple_times_do_succeed(
    bootstrap: &'static Bootstrap,
) -> Result<(), AllocationError> {
    let sku = "CONCURRENT_TABLE";
    let handle1 = std::thread::spawn(|| -> GenericResult<()> {
        try_to_allocate_multiple_times("c123", sku, bootstrap)
    });
    let handle2 = std::thread::spawn(|| -> GenericResult<()> {
        try_to_allocate_multiple_times("d123", sku, bootstrap)
    });

    let result = [handle1.join().unwrap(), handle2.join().unwrap()];
    assert!(result.iter().all(|r| r.is_ok()));

    let db_product = bootstrap
        .repo()
        .fetch_by_id(None, &ProductSku::from_str(sku)?)?;
    assert_eq!(4, db_product.version_number());

    cleanup(bootstrap.repo(), sku)
}
