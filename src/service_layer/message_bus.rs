use log::{debug, info};

use crate::{
    adapter::port::{MessageBus, Repository},
    domain::{
        command::Command,
        error::{AllocationError, ValidationError},
        event::Event,
        value_object::Message,
    },
    service_layer::handler::EventHandler,
    util,
};
use std::collections::{HashMap, VecDeque};

#[derive(Default)]
pub(crate) struct PoorManBus {
    // TODO: string as keys
    event_handler_map: HashMap<String, Vec<Box<dyn EventHandler>>>,
    command_handler_map: HashMap<String, Box<dyn EventHandler>>,
}

impl PoorManBus {
    pub(crate) fn new(
        event_handler_map: HashMap<String, Vec<Box<dyn EventHandler>>>,
        command_handler_map: HashMap<String, Box<dyn EventHandler>>,
    ) -> Self {
        Self {
            event_handler_map,
            command_handler_map,
        }
    }
}

impl MessageBus for PoorManBus {
    fn publish(&self, message: Message, repo: &dyn Repository) -> Result<(), AllocationError> {
        let mut queue: VecDeque<Message> = VecDeque::new();

        queue.push_back(message);
        while let Some(message) = queue.pop_front() {
            match message {
                Message::Event(event) => self.publish_event(event, &mut queue, repo),
                Message::Command(command) => self.publish_command(command, &mut queue, repo)?,
            };
        }
        Ok(())
    }
}

impl PoorManBus {
    fn publish_event(&self, event: Event, queue: &mut VecDeque<Message>, repo: &dyn Repository) {
        let num_retries = 3;
        let sleep_ms = 1;

        for handler in self
            .event_handler_map
            .get(&event.to_string())
            .into_iter()
            .flatten()
        {
            debug!("Publishing event: {:?}", event);
            let f = || {
                handler
                    .handle(&Message::Event(event.clone()), repo)
                    .map_err(|e| e.into())
            };
            match util::retry(num_retries, sleep_ms, f) {
                Ok(handler_response) => {
                    if let Some(aggregate_root) = &handler_response {
                        queue.extend(aggregate_root.messages().to_vec());
                    }
                }
                Err(e) => {
                    info!("Error handling event: {e:?}");
                }
            }
        }
    }

    fn publish_command(
        &self,
        command: Command,
        queue: &mut VecDeque<Message>,
        repo: &dyn Repository,
    ) -> Result<(), AllocationError> {
        debug!("Publishing command: {:?}", command);
        if let Some(handler) = self.command_handler_map.get(&command.to_string()) {
            if let Some(aggregate_root) = handler.handle(&Message::Command(command), repo)? {
                queue.extend(aggregate_root.messages().to_vec());
            }
            Ok(())
        } else {
            Err(
                ValidationError::new(format!("Non-existing command: {:?}", command.to_string()))
                    .into(),
            )
        }
    }
}
