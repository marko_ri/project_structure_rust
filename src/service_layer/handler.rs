#[cfg(test)]
mod test;

use crate::{
    adapter::port::{Notification, PubSub, PubSubTopic, Repository},
    domain::{
        command::{AllocateCmd, ChangeBatchQuantityCmd, Command, CreateBatchCmd},
        entity::{AggregateRoot, Batch, Product},
        error::{AllocationError, OutOfStockError},
        event::{AllocatedEvent, DeallocatedEvent, Event, OutOfStockEvent},
        value_object::{BatchReference, Message, OrderLine, ProductSku, Quantity},
    },
};
use std::str::FromStr;

pub(crate) trait EventHandler: Send + Sync {
    fn handle(
        &self,
        message: &Message,
        repo: &dyn Repository,
    ) -> Result<Option<AggregateRoot>, AllocationError>;
}

pub(crate) struct BatchCreatedHandler {}

impl BatchCreatedHandler {
    pub(crate) fn new() -> Self {
        Self {}
    }

    pub fn add_batch(
        &self,
        command: &CreateBatchCmd,
        repo: &dyn Repository,
    ) -> Result<Option<AggregateRoot>, AllocationError> {
        // tx
        let mut tx = repo.start_tx()?;

        let mut product =
            match repo.fetch_by_id(Some(&mut tx), &ProductSku::from_str(&command.sku)?) {
                Ok(val) => val,
                Err(_e) => Product::try_new(&command.sku, vec![], 0)?,
            };

        let batch = Batch::try_new(&command.id, &command.sku, &command.qty, command.eta.clone())?;
        product.add_batch(batch);

        repo.update(Some(&mut tx), &product)?;

        // tx
        repo.commit_tx(tx, &mut product)?;

        Ok(Some(product))
    }
}

impl EventHandler for BatchCreatedHandler {
    fn handle(
        &self,
        message: &Message,
        repo: &dyn Repository,
    ) -> Result<Option<AggregateRoot>, AllocationError> {
        match message {
            Message::Command(Command::CreateBatch(val)) => self.add_batch(val, repo),
            _ => unreachable!(),
        }
    }
}

pub(crate) struct AllocationRequiredHandler {}

impl AllocationRequiredHandler {
    pub(crate) fn new() -> Self {
        Self {}
    }

    pub fn allocate(
        &self,
        command: &AllocateCmd,
        repo: &dyn Repository,
    ) -> Result<Option<AggregateRoot>, AllocationError> {
        // tx
        let mut tx = repo.start_tx()?;

        let order_line = OrderLine::try_new(&command.order_id, &command.sku, &command.qty)?;
        let mut product: Product = repo.fetch_by_id(Some(&mut tx), &order_line.sku)?;
        if let Some(_batch_id) = product.allocate(&order_line) {
            repo.update(Some(&mut tx), &product)?;
        };

        // tx
        repo.commit_tx(tx, &mut product)?;

        Ok(Some(product))
    }
}

impl EventHandler for AllocationRequiredHandler {
    fn handle(
        &self,
        message: &Message,
        repo: &dyn Repository,
    ) -> Result<Option<AggregateRoot>, AllocationError> {
        match message {
            Message::Command(Command::Allocate(val)) => self.allocate(val, repo),
            _ => unreachable!(),
        }
    }
}

pub(crate) struct DeallocatedHandler {}

impl DeallocatedHandler {
    pub(crate) fn new() -> Self {
        Self {}
    }

    pub fn reallocate(
        &self,
        event: &DeallocatedEvent,
        repo: &dyn Repository,
    ) -> Result<Option<AggregateRoot>, AllocationError> {
        // tx
        let mut tx = repo.start_tx()?;

        let sku = ProductSku::from_str(&event.sku)?;
        let mut product: Product = repo.fetch_by_id(Some(&mut tx), &sku)?;
        product.add_message(Message::from(event.clone()));

        // tx
        repo.commit_tx(tx, &mut product)?;

        Ok(Some(product))
    }
}

impl EventHandler for DeallocatedHandler {
    fn handle(
        &self,
        message: &Message,
        repo: &dyn Repository,
    ) -> Result<Option<AggregateRoot>, AllocationError> {
        match message {
            Message::Event(Event::Deallocated(val)) => self.reallocate(val, repo),
            _ => unreachable!(),
        }
    }
}

pub(crate) struct AllocatedHandler<'a> {
    dependency: Option<&'a dyn PubSub>,
}
impl<'a> AllocatedHandler<'a> {
    pub(crate) fn new(dependency: Option<&'a dyn PubSub>) -> Self {
        Self { dependency }
    }

    fn send_to_pub_sub(
        &self,
        event: &AllocatedEvent,
        _repo: &dyn Repository,
    ) -> Result<Option<AggregateRoot>, AllocationError> {
        if let Some(pub_sub) = self.dependency {
            let msg = serde_json::to_string(event).unwrap();
            pub_sub.publish(&PubSubTopic::LineAllocated, msg)?;
        }
        Ok(None)
    }
}
impl<'a> EventHandler for AllocatedHandler<'a> {
    fn handle(
        &self,
        message: &Message,
        repo: &dyn Repository,
    ) -> Result<Option<AggregateRoot>, AllocationError> {
        match message {
            Message::Event(Event::Allocated(event)) => self.send_to_pub_sub(event, repo),
            _ => unreachable!(),
        }
    }
}

pub(crate) struct OutOfStockHandler<'a> {
    dependency: Option<&'a dyn Notification>,
}
impl<'a> OutOfStockHandler<'a> {
    pub(crate) fn new(dependency: Option<&'a dyn Notification>) -> Self {
        Self { dependency }
    }

    fn send_email(
        &self,
        event: &OutOfStockEvent,
        _repo: &dyn Repository,
    ) -> Result<Option<AggregateRoot>, AllocationError> {
        if let Some(notify) = self.dependency {
            notify.send("some_address", event.sku.to_string());
        }
        Ok(None)
    }
}
impl<'a> EventHandler for OutOfStockHandler<'a> {
    fn handle(
        &self,
        message: &Message,
        repo: &dyn Repository,
    ) -> Result<Option<AggregateRoot>, AllocationError> {
        match message {
            Message::Event(Event::OutOfStock(event)) => self.send_email(event, repo),
            _ => unreachable!(),
        }
    }
}

pub(crate) struct BatchQuantityChangedHandler {}
impl BatchQuantityChangedHandler {
    pub(crate) fn new() -> Self {
        Self {}
    }
}

impl BatchQuantityChangedHandler {
    fn change_batch_quantity(
        &self,
        command: &ChangeBatchQuantityCmd,
        repo: &dyn Repository,
    ) -> Result<Option<AggregateRoot>, AllocationError> {
        // tx
        let mut tx = repo.start_tx()?;

        let mut product: Product =
            repo.fetch_by_batch_id(Some(&mut tx), &BatchReference::from_str(&command.batch_id)?)?;

        if !product.change_batch_quantity(&command.batch_id, Quantity::from_str(&command.qty)?) {
            return Err(OutOfStockError::new(format!(
                "Can not change quantity for: {:?}",
                &command.batch_id
            ))
            .into());
        }

        repo.update(Some(&mut tx), &product)?;

        // tx
        repo.commit_tx(tx, &mut product)?;

        Ok(Some(product))
    }
}

impl EventHandler for BatchQuantityChangedHandler {
    fn handle(
        &self,
        message: &Message,
        repo: &dyn Repository,
    ) -> Result<Option<AggregateRoot>, AllocationError> {
        match message {
            Message::Command(Command::ChangeBatchQuantity(val)) => {
                self.change_batch_quantity(val, repo)
            }
            _ => unreachable!(),
        }
    }
}
