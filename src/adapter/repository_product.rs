use std::str::FromStr;

use crate::{
    adapter::{port::Transaction, repository::DbAdapter},
    domain::{
        entity::Product,
        error::DbError,
        value_object::{BatchReference, ProductSku},
    },
};
use log::debug;
use sqlx::Row;

impl DbAdapter {
    // should be one join... (anyway it is not really normalized schema:
    // sku is repeated everywhere)
    pub(crate) fn products_fetch_by_id(
        &self,
        executor: &mut Transaction,
        id: &ProductSku,
    ) -> Result<Product, DbError> {
        // fetch baches
        let batches = self.batches_fetch_by_sku(&mut *executor, id)?;

        // fetch product
        let q = "SELECT version_number FROM products WHERE id = ?1";
        let row = self
            .db
            .fetch_optional(executor, sqlx::query(q).bind(id.value()))?;

        match row {
            Some(row) => Product::try_new(id.value(), batches, row.get("version_number"))
                .map_err(|e| DbError::new(format!("Can not map product: {e:?}"))),
            None => Err(DbError::new(format!("No row with ID: {id:?}"))),
        }
    }

    pub(crate) fn products_insert(
        &self,
        executor: &mut Transaction,
        input: &Product,
    ) -> Result<(), DbError> {
        debug!("INSERT: {:?}", input);

        let q = "INSERT INTO products(id, version_number) VALUES(?1, ?2)";
        self.db.execute(
            &mut *executor,
            sqlx::query(q)
                .bind(input.id().value())
                .bind(input.version_number()),
        )?;

        for batch in input.batches() {
            self.batches_insert(executor, batch)?;
        }

        Ok(())
    }

    // TODO: for now: delete + insert
    pub(crate) fn products_update(
        &self,
        executor: &mut Transaction,
        input: &Product,
    ) -> Result<(), DbError> {
        // delete
        self.products_delete(&mut *executor, &input.id())?;

        // insert
        self.products_insert(executor, input)?;

        Ok(())
    }

    pub(crate) fn products_delete(
        &self,
        executor: &mut Transaction,
        id: &ProductSku,
    ) -> Result<u64, DbError> {
        debug!("DELETE: {:?}", id);

        // delete batches
        self.batches_delete_by_sku(&mut *executor, id)?;

        // delete products
        let q = "DELETE FROM products WHERE id = ?1";

        Ok(self
            .db
            .execute(&mut *executor, sqlx::query(q).bind(id.value()))?
            .rows_affected())
    }

    // Product (1)--(n) Batches (1) -- (n) OrderLines
    // TODO: instead of 3 queries, should be 1 join statement
    pub(crate) fn products_fetch_by_batch_id(
        &self,
        executor: &mut Transaction,
        batch_id: &BatchReference,
    ) -> Result<Product, DbError> {
        let q = "SELECT sku FROM batches WHERE id = ?1";
        let row = self
            .db
            .fetch_optional(&mut *executor, sqlx::query(q).bind(batch_id.value()))?;

        match row {
            Some(row) => self.products_fetch_by_id(
                executor,
                &ProductSku::from_str(row.get("sku"))
                    .map_err(|e| DbError::new(format!("Can not map ProductSku: {e:?}")))?,
            ),
            None => Err(DbError::new(format!("No row with ID: {batch_id:?}"))),
        }
    }

    pub(crate) fn products_update_version(
        &self,
        executor: &mut Transaction,
        id: &ProductSku,
    ) -> Result<u64, DbError> {
        let q = "UPDATE products SET version_number = version_number + 1 WHERE id = ?1";

        Ok(self
            .db
            .execute(&mut *executor, sqlx::query(q).bind(id.value()))?
            .rows_affected())
    }
}
