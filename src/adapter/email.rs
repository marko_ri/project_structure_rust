use log::info;

use super::port::Notification;
use std::{collections::HashSet, sync::RwLock};

pub(crate) struct Email {
    cached_emails: RwLock<HashSet<String>>,
}

impl Email {
    pub(crate) fn new() -> Self {
        Self {
            cached_emails: RwLock::new(HashSet::new()),
        }
    }
}

impl Notification for Email {
    fn send(&self, address: &str, body: String) -> bool {
        {
            self.cached_emails
                .write()
                .unwrap()
                .insert(address.to_string());
        }
        info!("Email sent to {address} for event: {body}");
        true
    }

    fn is_address_cached(&self, address: &str) -> bool {
        self.cached_emails.read().unwrap().contains(address)
    }
}
