use crate::{
    adapter::{port::Transaction, repository::DbAdapter},
    domain::{
        entity::Batch,
        error::DbError,
        value_object::{BatchReference, OrderLine, ProductSku},
    },
};
use sqlx::{sqlite::SqliteRow, Row};
use std::collections::HashMap;

impl TryFrom<&SqliteRow> for Batch {
    type Error = DbError;

    fn try_from(row: &SqliteRow) -> Result<Self, Self::Error> {
        let eta = match row.get::<Option<i32>, &str>("b_eta") {
            Some(e) => Some(u64::try_from(e).map_err(|e| DbError::new(format!("Error: {e:?}")))?),
            None => None,
        };
        Batch::try_new(
            row.get("b_id"),
            row.get("b_sku"),
            row.get("b_purchased_qty"),
            eta,
        )
        .map_err(|e| DbError::new(format!("Can not map batch: {e:?}")))
    }
}

impl DbAdapter {
    pub(crate) fn batches_fetch_by_sku(
        &self,
        executor: &mut Transaction,
        sku: &ProductSku,
    ) -> Result<Vec<Batch>, DbError> {
        let q = r#"
SELECT b.id as b_id, b.sku as b_sku, b.purchased_qty as b_purchased_qty, b.eta as b_eta, o.id as o_id, o.sku as o_sku, o.qty as o_qty, o.order_id as o_order_id
FROM batches b
LEFT JOIN order_lines o ON b.id = o.batch_id
WHERE b.sku = ?1
        "#;
        let rows = self
            .db
            .fetch_all(executor, sqlx::query(q).bind(sku.value()))?;
        let mut batches: HashMap<String, Batch> = HashMap::new();
        for row in rows.iter() {
            let batch_id = row.get::<&str, &str>("b_id");
            if !batches.contains_key(batch_id) {
                let batch = Batch::try_from(row)?;
                batches.insert(batch_id.to_string(), batch);
            }

            if row.get::<i64, &str>("o_id") > 0 {
                let order_line = OrderLine::try_from(row)?;
                let batch = batches.get_mut(batch_id).unwrap();
                batch.add_allocation(order_line);
            };
        }

        if batches.is_empty() {
            Err(DbError::new(format!("No batch with sku: {sku:?}")))
        } else {
            Ok(batches.into_values().collect())
        }
    }

    pub(crate) fn batches_insert(
        &self,
        executor: &mut Transaction,
        input: &Batch,
    ) -> Result<(), DbError> {
        // insert to batches
        let q = "INSERT INTO batches(id, sku, purchased_qty, eta) VALUES(?1, ?2, ?3, ?4)";
        self.db.execute(
            &mut *executor,
            sqlx::query(q)
                .bind(input.id.value())
                .bind(input.sku.value())
                .bind(input.purchased_qty.to_string())
                .bind(input.eta.as_ref().map(|eta| eta.to_secs() as i64)),
        )?;

        // insert to order_lines
        for order_line in input.allocations().iter() {
            self.order_lines_insert(executor, order_line, &input.id)?;
        }

        Ok(())
    }

    pub(crate) fn batches_fetch_by_id(
        &self,
        executor: &mut Transaction,
        id: &BatchReference,
    ) -> Result<Batch, DbError> {
        let q = r#"
SELECT b.id as b_id, b.sku as b_sku, b.purchased_qty as b_purchased_qty, b.eta as b_eta, o.id as o_id, o.sku as o_sku, o.qty as o_qty, o.order_id as o_order_id
FROM batches b
LEFT JOIN order_lines o ON b.id = o.batch_id
WHERE b.id = ?1
        "#;
        let rows = self
            .db
            .fetch_all(executor, sqlx::query(q).bind(id.value()))?;
        let mut batch = None;
        for row in rows.iter() {
            if batch.is_none() {
                batch = Some(Batch::try_from(row)?);
            }

            if row.get::<i64, &str>("o_id") > 0 {
                let order_line = OrderLine::try_from(row)?;
                batch.as_mut().unwrap().add_allocation(order_line);
            };
        }

        batch.ok_or_else(|| DbError::new(format!("No batch with id: {id:?}")))
    }

    pub(crate) fn batches_delete_by_sku(
        &self,
        executor: &mut Transaction,
        sku: &ProductSku,
    ) -> Result<u64, DbError> {
        // delete from order_lines
        self.order_lines_delete_by_sku(&mut *executor, sku)?;

        // delete from batches
        let q = "DELETE FROM batches WHERE sku = ?1";
        let rows_affected = self
            .db
            .execute(executor, sqlx::query(q).bind(sku.value()))?
            .rows_affected();

        Ok(rows_affected)
    }

    pub(crate) fn batches_delete_by_id(
        &self,
        executor: &mut Transaction,
        id: &BatchReference,
    ) -> Result<u64, DbError> {
        // delete from order_lines
        self.order_lines_delete_by_batch_id(&mut *executor, id)?;

        // delete from batches
        let q = "DELETE FROM batches WHERE id = ?1";
        let rows_affected = self
            .db
            .execute(executor, sqlx::query(q).bind(id.value()))?
            .rows_affected();

        Ok(rows_affected)
    }

    // TODO: for now: delete + insert
    pub(crate) fn batches_update(
        &self,
        executor: &mut Transaction,
        input: &Batch,
    ) -> Result<(), DbError> {
        // delete
        self.batches_delete_by_id(&mut *executor, &input.id)?;

        // insert
        self.batches_insert(executor, input)?;

        Ok(())
    }
}
