use std::time::Duration;

use crate::domain::{
    entity::Product,
    error::{AllocationError, DbError, MessageBusError},
    value_object::{BatchReference, Message, OrderLineView, ProductSku},
};

pub(crate) type Transaction = sqlx::Transaction<'static, sqlx::Sqlite>;

// TODO: external calls should have timeout values ?
pub trait Notification: Send + Sync {
    fn send(&self, address: &str, body: String) -> bool;
    fn is_address_cached(&self, address: &str) -> bool;
}

// MessageBus is used for internal messages
pub trait MessageBus: Send + Sync {
    fn publish(&self, message: Message, repo: &dyn Repository) -> Result<(), AllocationError>;
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Clone)]
pub enum PubSubTopic {
    ChangeBatchQuantity,
    LineAllocated,
}

pub type PubSubContent = String;

// PubSub is used for external messages
pub trait PubSub: Send + Sync {
    fn subscribe(&self, topic: &PubSubTopic) -> Box<dyn Subscription>;
    fn publish(&self, topic: &PubSubTopic, message: PubSubContent) -> Result<(), MessageBusError>;
}

pub trait Subscription: Send {
    fn get_message(&self) -> Result<PubSubContent, MessageBusError>;
    fn get_message_blocking(&self, timeout: Duration) -> Result<PubSubContent, MessageBusError>;
}

pub trait ExposedApi {
    fn add_batch(&self, json_in: &str) -> String;
    fn allocate(&self, json_in: &str) -> String;
    fn handle_change_batch_quantity(&self) -> String;
    fn get_allocation(&self, order_id: &str) -> String;
}

pub trait Repository: Send + Sync {
    fn start_tx(&self) -> Result<Transaction, DbError>;
    fn commit_tx(&self, tx: Transaction, input: &mut Product) -> Result<(), DbError>;
    fn fetch_by_id(
        &self,
        tx: Option<&mut Transaction>,
        id: &ProductSku,
    ) -> Result<Product, DbError>;
    fn insert(&self, tx: Option<&mut Transaction>, input: &Product) -> Result<(), DbError>;
    fn update(&self, tx: Option<&mut Transaction>, input: &Product) -> Result<(), DbError>;
    fn delete(&self, tx: Option<&mut Transaction>, id: &ProductSku) -> Result<u64, DbError>;
    fn fetch_by_batch_id(
        &self,
        dbtx: Option<&mut Transaction>,
        batch_id: &BatchReference,
    ) -> Result<Product, DbError>;
    // TODO: operations on "views" probably do not belong here
    fn orderlineview_fetch_by_order_id(
        &self,
        order_id: &str,
    ) -> Result<Vec<OrderLineView>, DbError>;
}
