use log::debug;

use crate::{
    adapter::port::{Repository, Transaction},
    domain::{
        entity::Product,
        error::DbError,
        value_object::{BatchReference, OrderLineView, ProductSku},
    },
    util::db_wrapper::SqlxSync,
};

pub(crate) struct DbAdapter {
    pub(crate) db: SqlxSync,
}

impl DbAdapter {
    fn new(db_url: &str) -> Self {
        Self {
            db: SqlxSync::connect(db_url).expect("DB connection"),
        }
    }

    fn migrate(&self) {
        self.db.migrate().expect("DB migration");
    }

    fn start_tx(&self) -> Result<Transaction, DbError> {
        self.db.start_tx().map_err(DbError::from)
    }

    fn commit_tx(&self, tx: Transaction) -> Result<(), DbError> {
        self.db.commit_tx(tx).map_err(DbError::from)
    }

    fn wrap_in_tx<F, I, O>(
        &self,
        dbtx: Option<&mut Transaction>,
        input: I,
        f: F,
    ) -> Result<O, DbError>
    where
        F: Fn(&DbAdapter, &mut Transaction, I) -> Result<O, DbError>,
    {
        let mut tx_new = match dbtx {
            Some(_) => None,
            None => Some(self.start_tx()?),
        };
        let executor = dbtx.unwrap_or_else(|| tx_new.as_mut().unwrap());

        let result = f(&self, executor, input)?;

        if let Some(tx) = tx_new {
            self.commit_tx(tx)?
        };

        Ok(result)
    }
}

pub(crate) struct AggregateRepo {
    db_adapter: DbAdapter,
}
impl AggregateRepo {
    pub(crate) fn new(db_url: &str) -> Self {
        let db_adapter = DbAdapter::new(db_url);
        db_adapter.migrate();
        Self { db_adapter }
    }
}

impl Repository for AggregateRepo {
    fn start_tx(&self) -> Result<Transaction, DbError> {
        self.db_adapter.start_tx()
    }

    fn commit_tx(&self, mut tx: Transaction, input: &mut Product) -> Result<(), DbError> {
        debug!(
            "COMMITING TRANSACTION: {:?}-{:?}",
            std::thread::current().id(),
            input.version_number() + 1
        );
        self.db_adapter
            .products_update_version(&mut tx, input.id())?;
        self.db_adapter.commit_tx(tx)?;
        input.increment_version();
        Ok(())
    }

    fn fetch_by_id(
        &self,
        dbtx: Option<&mut Transaction>,
        input: &ProductSku,
    ) -> Result<Product, DbError> {
        self.db_adapter
            .wrap_in_tx(dbtx, input, DbAdapter::products_fetch_by_id)
    }

    fn insert(&self, dbtx: Option<&mut Transaction>, input: &Product) -> Result<(), DbError> {
        self.db_adapter
            .wrap_in_tx(dbtx, input, DbAdapter::products_insert)
    }

    fn update(&self, dbtx: Option<&mut Transaction>, input: &Product) -> Result<(), DbError> {
        self.db_adapter
            .wrap_in_tx(dbtx, input, DbAdapter::products_update)
    }

    fn delete(&self, dbtx: Option<&mut Transaction>, id: &ProductSku) -> Result<u64, DbError> {
        self.db_adapter
            .wrap_in_tx(dbtx, id, DbAdapter::products_delete)
    }

    fn fetch_by_batch_id(
        &self,
        dbtx: Option<&mut Transaction>,
        batch_id: &BatchReference,
    ) -> Result<Product, DbError> {
        self.db_adapter
            .wrap_in_tx(dbtx, batch_id, DbAdapter::products_fetch_by_batch_id)
    }

    fn orderlineview_fetch_by_order_id(
        &self,
        order_id: &str,
    ) -> Result<Vec<OrderLineView>, DbError> {
        self.db_adapter
            .wrap_in_tx(None, order_id, DbAdapter::orderlineview_fetch_by_order_id)
    }
}
