use crate::{
    adapter::{port::Transaction, repository::DbAdapter},
    domain::{
        error::DbError,
        value_object::{BatchReference, OrderLine, OrderLineView, ProductSku},
    },
};
use log::debug;
use sqlx::{sqlite::SqliteRow, Row};

impl TryFrom<&SqliteRow> for OrderLine {
    type Error = DbError;

    fn try_from(row: &SqliteRow) -> Result<Self, Self::Error> {
        OrderLine::try_new(row.get("o_order_id"), row.get("o_sku"), row.get("o_qty"))
            .map_err(|e| DbError::new(format!("Can not map order_line: {e:?}")))
    }
}

impl DbAdapter {
    pub(crate) fn order_lines_fetch_by_batch_id(
        &self,
        executor: &mut Transaction,
        batch_id: &BatchReference,
    ) -> Result<Vec<OrderLine>, DbError> {
        let q = "SELECT sku as o_sku, qty as o_qty, order_id as o_order_id FROM order_lines WHERE batch_id = ?1";

        let rows = self
            .db
            .fetch_all(executor, sqlx::query(q).bind(batch_id.value()))?;

        rows.iter().map(|row| OrderLine::try_from(row)).collect()
    }

    pub(crate) fn order_lines_delete_by_batch_id(
        &self,
        executor: &mut Transaction,
        batch_id: &BatchReference,
    ) -> Result<u64, DbError> {
        let q = "DELETE FROM order_lines WHERE batch_id = ?1";
        let rows_affected = self
            .db
            .execute(executor, sqlx::query(q).bind(batch_id.value()))?
            .rows_affected();

        Ok(rows_affected)
    }

    pub(crate) fn order_lines_delete_by_sku(
        &self,
        executor: &mut Transaction,
        sku: &ProductSku,
    ) -> Result<u64, DbError> {
        let q = "DELETE FROM order_lines WHERE sku = ?1";
        let rows_affected = self
            .db
            .execute(executor, sqlx::query(q).bind(sku.value()))?
            .rows_affected();

        Ok(rows_affected)
    }

    pub(crate) fn order_lines_insert(
        &self,
        executor: &mut Transaction,
        input: &OrderLine,
        batch_id: &BatchReference,
    ) -> Result<i64, DbError> {
        let q = "INSERT INTO order_lines(sku, qty, order_id, batch_id) VALUES(?1, ?2, ?3, ?4)";
        Ok(self
            .db
            .execute(
                executor,
                sqlx::query(q)
                    .bind(input.sku.value())
                    .bind(input.qty.to_string())
                    .bind(input.order_id.value())
                    .bind(batch_id.value()),
            )?
            .last_insert_rowid())
    }

    pub(crate) fn orderlineview_fetch_by_order_id(
        &self,
        executor: &mut Transaction,
        order_id: &str,
    ) -> Result<Vec<OrderLineView>, DbError> {
        debug!("VIEW_FETCH: {:?}", order_id);
        let q = "SELECT sku, batch_id FROM order_lines WHERE order_id = ?1";

        let rows = self.db.fetch_all(executor, sqlx::query(q).bind(order_id))?;

        rows.iter()
            .map(|row| {
                Ok(OrderLineView {
                    sku: row.get("sku"),
                    batch_id: row.get("batch_id"),
                })
            })
            .collect()
    }
}

#[cfg(test)]
mod test {
    use crate::{
        adapter::port::Repository,
        bootstrap::Bootstrap,
        domain::{
            command::{AllocateCmd, ChangeBatchQuantityCmd, CreateBatchCmd},
            error::AllocationError,
            value_object::{DeliveryTime, Message, OrderLineView, ProductSku},
        },
    };
    use once_cell::sync::OnceCell;
    use std::str::FromStr;

    static BOOTSTRAP: OnceCell<Bootstrap> = OnceCell::new();

    fn bootstrap() -> Bootstrap {
        // because tests run in parallel and we are sharing the same sqlite connection, run these in sequence.
        // (if not-in-memory db is used, run tests with: cargo test -- --test-threads=1)
        // "Note that the journal_mode for an in-memory database is either MEMORY or OFF and can not be changed to a different value."
        Bootstrap::new("DATABASE_URL_TEST")
    }

    fn cleanup(repo: &dyn Repository, skus: &[&str]) -> Result<(), AllocationError> {
        for sku in skus {
            let rows_affected = repo.delete(None, &ProductSku::from_str(sku)?)?;
            assert_eq!(1, rows_affected);
        }

        Ok(())
    }

    #[test]
    fn test_view_sequentially() -> Result<(), AllocationError> {
        let bootstrap = BOOTSTRAP.get_or_init(bootstrap);

        test_allocations_view(&bootstrap)?;
        test_deallocation_view(&bootstrap)?;

        Ok(())
    }

    fn test_allocations_view(bootstrap: &Bootstrap) -> Result<(), AllocationError> {
        let today = DeliveryTime::now().plus_secs(60);
        let sku1 = "sku1";
        let sku2 = "sku2";

        bootstrap.bus().publish(
            Message::from(CreateBatchCmd::new("sku1batch", sku1, "U50", None)),
            &*bootstrap.repo(),
        )?;
        bootstrap.bus().publish(
            Message::from(CreateBatchCmd::new(
                "sku2batch",
                sku2,
                "U50",
                Some(today.to_secs()),
            )),
            &*bootstrap.repo(),
        )?;
        bootstrap.bus().publish(
            Message::from(AllocateCmd::new("order1", sku1, "U20")),
            &*bootstrap.repo(),
        )?;
        bootstrap.bus().publish(
            Message::from(AllocateCmd::new("order1", sku2, "U20")),
            &*bootstrap.repo(),
        )?;
        // add a spurious batch and order to make sure we're getting the right ones
        bootstrap.bus().publish(
            Message::from(CreateBatchCmd::new(
                "sku1batch-later",
                sku1,
                "U50",
                Some(today.to_secs()),
            )),
            &*bootstrap.repo(),
        )?;
        bootstrap.bus().publish(
            Message::from(AllocateCmd::new("otherorder", sku1, "U30")),
            &*bootstrap.repo(),
        )?;
        bootstrap.bus().publish(
            Message::from(AllocateCmd::new("otherorder", sku2, "U10")),
            &*bootstrap.repo(),
        )?;

        let views = bootstrap.repo().orderlineview_fetch_by_order_id("order1")?;
        let expected1 = OrderLineView {
            sku: sku1.to_string(),
            batch_id: "sku1batch".to_string(),
        };
        let expected2 = OrderLineView {
            sku: sku2.to_string(),
            batch_id: "sku2batch".to_string(),
        };
        assert_eq!(2, views.len());
        assert!(views.contains(&expected1));
        assert!(views.contains(&expected2));

        cleanup(bootstrap.repo(), &[sku1, sku2])
    }

    fn test_deallocation_view(bootstrap: &Bootstrap) -> Result<(), AllocationError> {
        let today = DeliveryTime::now().plus_secs(60);
        let sku = "sku3";

        bootstrap.bus().publish(
            Message::from(CreateBatchCmd::new("batch1", sku, "U50", None)),
            &*bootstrap.repo(),
        )?;
        bootstrap.bus().publish(
            Message::from(CreateBatchCmd::new(
                "batch2",
                sku,
                "U50",
                Some(today.to_secs()),
            )),
            &*bootstrap.repo(),
        )?;
        bootstrap.bus().publish(
            Message::from(AllocateCmd::new("o_id1", sku, "U40")),
            &*bootstrap.repo(),
        )?;
        bootstrap.bus().publish(
            Message::from(ChangeBatchQuantityCmd::new("batch1", "U10")),
            &*bootstrap.repo(),
        )?;

        let views = bootstrap.repo().orderlineview_fetch_by_order_id("o_id1")?;
        let expected1 = OrderLineView {
            sku: sku.to_string(),
            batch_id: "batch2".to_string(),
        };
        assert_eq!(1, views.len());
        assert!(views.contains(&expected1));

        cleanup(bootstrap.repo(), &[sku])
    }
}
