use log::debug;

use crate::adapter::port::{PubSub, PubSubContent, PubSubTopic, Subscription};
use crate::domain::error::MessageBusError;

use std::collections::HashMap;
use std::sync::{mpsc, Mutex};
use std::time::Duration;

#[derive(Debug)]
pub struct PoorManPubSub {
    senders: Mutex<HashMap<PubSubTopic, Vec<mpsc::Sender<PubSubContent>>>>,
}

impl PoorManPubSub {
    pub fn new() -> Self {
        Self {
            senders: Mutex::new(HashMap::new()),
        }
    }
}

impl PubSub for PoorManPubSub {
    fn subscribe(&self, topic: &PubSubTopic) -> Box<dyn Subscription> {
        let (send, recv) = mpsc::channel();
        {
            let mut senders = self.senders.lock().unwrap();
            senders
                .entry(topic.clone())
                .or_insert_with(|| Vec::new())
                .push(send);
        }

        Box::new(PoorManSubscriber { receiver: recv })
    }

    fn publish(&self, topic: &PubSubTopic, message: PubSubContent) -> Result<(), MessageBusError> {
        debug!("PubSub publishing msg: {message:?} on: {topic:?}");
        let senders = self.senders.lock().unwrap();

        for sender in senders.get(topic).into_iter().flatten() {
            sender.send(message.clone())?;
        }

        Ok(())
    }
}

pub(crate) struct PoorManSubscriber {
    receiver: mpsc::Receiver<PubSubContent>,
}

impl Subscription for PoorManSubscriber {
    // Tries to receive a single message, not blocking
    fn get_message(&self) -> Result<PubSubContent, MessageBusError> {
        self.receiver.try_recv().map_err(|e| e.into())
    }

    fn get_message_blocking(&self, timeout: Duration) -> Result<PubSubContent, MessageBusError> {
        self.receiver.recv_timeout(timeout).map_err(|e| e.into())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::domain::error::MessageBusError;

    #[test]
    fn test_pub_sub_single_thread() -> Result<(), MessageBusError> {
        let topic = PubSubTopic::ChangeBatchQuantity;
        let pub_sub = PoorManPubSub::new();
        let recv = pub_sub.subscribe(&topic);

        let message = "message01";
        pub_sub.publish(&topic, message.to_string())?;
        let actual = recv.get_message()?;
        assert_eq!(message, actual);

        Ok(())
    }

    #[test]
    fn test_pub_sub_multi_thread() -> Result<(), MessageBusError> {
        use std::sync::Arc;

        let topics = [PubSubTopic::ChangeBatchQuantity, PubSubTopic::LineAllocated];
        let pub_sub = Arc::new(PoorManPubSub::new());

        let mut handles = Vec::new();

        for topic in topics.iter().cloned() {
            let pub_sub = pub_sub.clone();
            let handle = std::thread::spawn(move || {
                let recv = pub_sub.subscribe(&topic);
                println!(
                    "received: {:?} on thread: {:?}",
                    recv.get_message_blocking(Duration::from_millis(3)),
                    std::thread::current().id()
                );
            });
            handles.push(handle);
        }

        for topic in topics.iter().cloned() {
            let pub_sub = pub_sub.clone();
            let handle = std::thread::spawn(move || {
                let msg = format!("sent from thread{:?}", std::thread::current().id());
                if let Err(e) = pub_sub.publish(&topic, msg) {
                    println! {"ERROR: {e:?}"};
                }
            });
            handles.push(handle);
        }

        for handle in handles.into_iter() {
            handle.join().unwrap();
        }

        Ok(())
    }
}
