use log::info;

use crate::{
    adapter::{
        port::{MessageBus, Notification, PubSub, Repository},
        repository::AggregateRepo,
    },
    service_layer::{
        handler::{
            AllocatedHandler, AllocationRequiredHandler, BatchCreatedHandler,
            BatchQuantityChangedHandler, DeallocatedHandler, EventHandler, OutOfStockHandler,
        },
        message_bus::PoorManBus,
    },
    util,
};
use std::collections::HashMap;

// state shared by multiple threads
// aggregate_repo is gate to db which deals with locking,
// message_bus has only immutable hashmaps
pub struct Bootstrap {
    repo: Box<dyn Repository>,
    bus: Box<dyn MessageBus>,
}

impl Bootstrap {
    pub fn new(env_key: &str) -> Self {
        Self::init(env_key, None, None)
    }

    pub fn new_with_pub_sub(env_key: &str, pub_sub: &'static dyn PubSub) -> Self {
        Self::init(env_key, Some(pub_sub), None)
    }

    pub fn new_with_notification(env_key: &str, notification: &'static dyn Notification) -> Self {
        Self::init(env_key, None, Some(notification))
    }

    // TODO: builder pattern
    fn init(
        env_key: &str,
        pub_sub: Option<&'static dyn PubSub>,
        notification: Option<&'static dyn Notification>,
    ) -> Self {
        util::read_env_vars(".env").expect(".env file");
        let _ignore = env_logger::try_init();
        info!("Bootstrapping the app....");
        let db_url = std::env::var(env_key).expect(&format!("missing key: {:?}", env_key));

        Self {
            repo: Box::new(AggregateRepo::new(&db_url)),
            bus: Box::new(PoorManBus::new(
                init_event_handler_map(pub_sub, notification),
                init_command_handler_map(),
            )),
        }
    }

    pub fn repo(&self) -> &dyn Repository {
        &*self.repo
    }

    pub fn bus(&self) -> &dyn MessageBus {
        &*self.bus
    }
}

fn init_event_handler_map<'a>(
    pub_sub: Option<&'a dyn PubSub>,
    notification: Option<&'a dyn Notification>,
) -> HashMap<String, Vec<Box<dyn EventHandler + 'a>>> {
    let mut result: HashMap<String, Vec<Box<dyn EventHandler>>> = HashMap::new();
    result.insert(
        "OutOfStock".to_string(),
        vec![Box::new(OutOfStockHandler::new(notification))],
    );
    result.insert(
        "Allocated".to_string(),
        vec![Box::new(AllocatedHandler::new(pub_sub))],
    );
    result.insert(
        "Deallocated".to_string(),
        vec![Box::new(DeallocatedHandler::new())],
    );
    result
}

fn init_command_handler_map() -> HashMap<String, Box<dyn EventHandler>> {
    let mut result: HashMap<String, Box<dyn EventHandler>> = HashMap::new();
    result.insert(
        "Allocate".to_string(),
        Box::new(AllocationRequiredHandler::new()),
    );
    result.insert(
        "CreateBatch".to_string(),
        Box::new(BatchCreatedHandler::new()),
    );
    result.insert(
        "ChangeBatchQuantity".to_string(),
        Box::new(BatchQuantityChangedHandler::new()),
    );
    result
}
