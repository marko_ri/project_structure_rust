use once_cell::sync::OnceCell;
use project_structure_rust::{
    adapter::{
        port::{ExposedApi, PubSub, PubSubTopic, Repository},
        pub_sub::PoorManPubSub,
    },
    bootstrap::Bootstrap,
    domain::{
        command::{AllocateCmd, ChangeBatchQuantityCmd, CreateBatchCmd},
        error::{AllocationError, GenericResult},
        event::AllocatedEvent,
        value_object::{DeliveryTime, OrderLineView, ProductSku},
    },
    entrypoint::api_provider::{ApiResponse, PoorManApiProvider},
};
use std::str::FromStr;

static PUB_SUB: OnceCell<Box<dyn PubSub>> = OnceCell::new();
static BOOTSTRAP: OnceCell<Bootstrap> = OnceCell::new();

fn bootstrap() -> Bootstrap {
    let pub_sub = PUB_SUB.get_or_init(|| Box::new(PoorManPubSub::new()));
    // because tests run in parallel and we are sharing the same sqlite connection, run these in sequence.
    // (if not-in-memory db is used, run tests with: cargo test -- --test-threads=1)
    // "Note that the journal_mode for an in-memory database is either MEMORY or OFF and can not be changed to a different value."
    Bootstrap::new_with_pub_sub("DATABASE_URL_TEST", pub_sub.as_ref())
}

fn cleanup(repo: &dyn Repository, sku: &str) -> Result<(), AllocationError> {
    let rows_affected = repo.delete(None, &ProductSku::from_str(sku)?)?;
    assert_eq!(1, rows_affected);

    Ok(())
}

#[test]
fn test_change_batch_quantity_leading_to_reallocation() -> GenericResult<()> {
    // external pub_sub service
    let pub_sub = PUB_SUB.get_or_init(|| Box::new(PoorManPubSub::new()));
    // external message_bus and repository service
    let bootstrap = BOOTSTRAP.get_or_init(bootstrap);
    // implements ExposedApi
    let mut api_provider = PoorManApiProvider::new(bootstrap);

    let sku = "CHAIR";
    let now = DeliveryTime::now();
    let later = now.plus_secs(60);

    let cmd_batch_old = CreateBatchCmd::new("old", sku, "U10", Some(now.to_secs()));
    let cmd_batch_newer = CreateBatchCmd::new("newer", sku, "U10", Some(later.to_secs()));
    api_provider.add_batch(&serde_json::to_string(&cmd_batch_old)?);
    api_provider.add_batch(&serde_json::to_string(&cmd_batch_newer)?);

    let cmd_allocate = AllocateCmd::new("aloc_order_id", sku, "U10");
    let res = api_provider.allocate(&serde_json::to_string(&cmd_allocate)?);
    let res: ApiResponse = serde_json::from_str(&res)?;
    assert_eq!(200, res.status_code);
    let res = api_provider.get_allocation("aloc_order_id");
    let res: ApiResponse = serde_json::from_str(&res)?;
    assert_eq!(200, res.status_code);
    let views: Vec<OrderLineView> = serde_json::from_str(&res.message)?;
    assert_eq!("old", views[0].batch_id);

    api_provider.subscribe(&PubSubTopic::ChangeBatchQuantity, pub_sub.as_ref());
    api_provider.subscribe(&PubSubTopic::LineAllocated, pub_sub.as_ref());

    // change quantity on allocated batch so it's less than our order
    let cmd_change_batch_qty = ChangeBatchQuantityCmd::new("old", "U5");
    pub_sub.publish(
        &PubSubTopic::ChangeBatchQuantity,
        serde_json::to_string(&cmd_change_batch_qty)?,
    )?;

    let mut messages = Vec::new();
    let message = api_provider.handle_change_batch_quantity();
    messages.push(message);
    let message = api_provider.retrieve_from_channel(&PubSubTopic::LineAllocated)?;
    messages.push(message);
    let data = messages.last().unwrap();
    let data: AllocatedEvent = serde_json::from_str(data)?;
    assert_eq!("aloc_order_id", data.order_id);
    assert_eq!("newer", data.batch_id);

    cleanup(bootstrap.repo(), sku).map_err(|e| e.to_string().into())
}

#[test]
fn test_not_visible_externaly() {
    // these should be compiler errors
}
